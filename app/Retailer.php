<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    protected $fillable = [
        'name', 'image_white', 'image_black', 'link', 'general'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'general' => 'boolean',
    ];

    public function productRetailers()
    {
        return $this->hasMany('App\ProductRetailer');
    }

    public function stateRetailers()
    {
        return $this->hasMany('App\StateRetailer');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutItem extends Model
{
    protected $fillable = [
        'age', 'description', 'image'
    ];
}

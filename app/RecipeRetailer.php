<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeRetailer extends Model
{
    protected $fillable = [
        'link',
    ];

    public function recipe()
    {
        return $this->belongsTo('App\Recipe');
    }

    public function retailer()
    {
        return $this->belongsTo('App\Retailer');
    }
}

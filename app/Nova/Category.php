<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Category extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Category::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Productos';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name'),
            Text::make('Presentation'),
            Text::make('Name_secondary'),
            Text::make('Slug'),
            Text::make('About_title'),
            Textarea::make('About'),
            Text::make('Legal'),
            Textarea::make('Creation'),
            Text::make('Creation_item1_title'),
            Textarea::make('Creation_item1_content'),
            Image::make('Creation_item1_image'),
            Text::make('Creation_item2_title'),
            Textarea::make('Creation_item2_content'),
            Image::make('Creation_item2_image'),
            Text::make('Creation_item3_title'),
            Textarea::make('Creation_item3_content'),
            Image::make('Creation_item3_image'),
            Image::make('Image_home'),
            Image::make('Image_products'),
            Image::make('Image_bg'),
            Image::make('Image_stamp'),

            HasMany::make('Products'),
            
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'image', 'image_thumb',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function productRetailers()
    {
        return $this->hasMany('App\ProductRetailer');
    }

    public function recipeProducts()
    {
        return $this->hasMany('App\RecipeProduct');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'name'
    ];

    public function productRetailers()
    {
        return $this->hasMany('App\ProductRetailer');
    }

    public function stateRetailers()
    {
        return $this->hasMany('App\StateRetailer');
    }
}

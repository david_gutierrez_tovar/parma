<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Celebration extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'name_close',
        'invitation_web',
        'invitation',
        'text_share',
        'meta_title',
        'meta_description',
        'image_share'
    ];


    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }

}
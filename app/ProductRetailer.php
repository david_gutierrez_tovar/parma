<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRetailer extends Model
{
    protected $fillable = [
        'link',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function retailer()
    {
        return $this->belongsTo('App\Retailer');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
}

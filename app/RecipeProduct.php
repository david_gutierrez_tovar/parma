<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeProduct extends Model
{

    public function recipe()
    {
        return $this->belongsTo('App\Recipe');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}

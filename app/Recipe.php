<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = [
        'name',
        'full_name',
        'slug',
        'difficulty',
        'time',
        'introduction'.
        'ingredients',
        'process',
        'tip',
        'image_thumb',
        'image_main',
        'video_link',
        'ingredients_link',
        'recipe_pdf'
    ];

    public function recipeIngredients()
    {
        return $this->hasMany('App\RecipeIngredient');
    }

    public function recipeProducts()
    {
        return $this->hasMany('App\RecipeProduct');
    }

    public function recipeRetailers()
    {
        return $this->hasMany('App\RecipeRetailer');
    }

}

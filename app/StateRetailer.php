<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateRetailer extends Model
{
    protected $fillable = [
        'link',
    ];

    public function retailer()
    {
        return $this->belongsTo('App\Retailer');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
}

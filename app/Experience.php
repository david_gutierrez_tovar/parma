<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        'name',
    ];

    public function celebration()
    {
        return $this->belongsTo('App\Celebration');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }


}

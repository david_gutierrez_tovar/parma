<?php

namespace App\Http\Controllers;

use App\Celebration;
use App\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExperienceController extends Controller
{

    public function index()
    {
        $celebrations = Celebration::all();

        return view('public.experience.index', compact('celebrations'));
    }

    public function create(Request $request)
    {
        $experience = new Experience;

        $experience->name = strtoupper($request->name);
        $experience->category_id = $request->category_id;
        $experience->celebration_id = $request->celebration_id;
        $experience->save();

        return $experience->id;
    }

    public function show(Celebration $celebration, Request $request)
    {
        $experience = Experience::whereId($request->id)->first();

        return view('public.experience.show', compact('celebration', 'experience'));

        
    }

}

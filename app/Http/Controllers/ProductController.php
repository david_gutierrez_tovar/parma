<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('public.products.index', compact('categories'));
    }

    public function showCategory(Category $category)
    {
        $products = $category->products()->get();

        return view('public.products.category', compact('category', 'products'));
    }

    public function showProduct(Category $category, Product $product)
    {

        $relatedProducts = Product::whereCategoryId($category->id)
            ->where('id', '!=', $product->id)
            ->get();


        return view('public.products.show', compact('product', 'relatedProducts'));
    }

    public function getProducts(Category $category)
    {   
        $products = Product::whereCategoryId($category->id)->get();

        return ['products' => $products, 'categorySlug' => $category->slug];
    }

    public function getProduct(Recipe $recipe)
    {   

        $products = DB::table('products')
                    ->join('recipe_products', 'recipe_products.product_id', '=', 'products.id')
                    ->select('products.*')
                    ->where('recipe_products.recipe_id', '=', $recipe->id)
                    ->get();

        return $products;
    }
}

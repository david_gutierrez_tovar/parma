<?php

namespace App\Http\Controllers;

use App\AboutItem;
use App\Category;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $categories = Category::all();

        return view('public.home', compact('categories'));
    }

    public function about()
    {
        $about_items = AboutItem::all();

        return view('public.about', compact('about_items'));
    }
    
    public function contact()
    {
        return view('public.contact');
    }

    public function sendMessage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'telephone' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }



        $message = new Message();

        $message->name = $request->name;
        $message->last_name = $request->last_name;
        $message->telephone = $request->telephone;
        $message->email = $request->email;
        $message->message = $request->message;

        $message->save();

        return back()->with('success','Mensaje enviado correctamente'); 
    }
}

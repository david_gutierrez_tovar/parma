<?php

namespace App\Http\Controllers;

use App\Category;
use App\Nova\Product;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    public function index()
    {
        $recipes = Recipe::all();

        return view('public.recipes.index', compact('recipes'));
    }

    public function show(Recipe $recipe)
    {
        return view('public.recipes.show', compact('recipe'));
    }

    public function getRecipes(Category $category)
    {   
        $recipes = DB::table('recipes')
                    ->join('recipe_products', 'recipe_products.recipe_id', '=', 'recipes.id')
                    ->join('products', 'products.id', '=', 'recipe_products.product_id')
                    ->join('categories', 'categories.id', '=', 'products.category_id')
                    ->select('recipes.*')
                    ->where('categories.id', '=', $category->id)
                    ->get();

        return $recipes;
    }

    public function getRecipe(Category $category)
    {   
        $recipe = Recipe::whereCategoryId($category->id)->get()->shuffle()->first();

        return $recipe;
    }

    public function getRecipeId(Recipe $recipe)
    {   
        return $recipe;
    }
}

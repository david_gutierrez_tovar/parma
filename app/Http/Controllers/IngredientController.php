<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Recipe;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    public function getIngredients(Recipe $recipe)
    {   
        $ingredients = [];
        
        foreach ($recipe->recipeIngredients as $key => $recipeIngredient) {
            array_push($ingredients, $recipeIngredient->ingredient);
        }

        return $ingredients;
    }
}

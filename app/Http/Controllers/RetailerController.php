<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductRetailer;
use App\Retailer;
use App\State;
use App\StateRetailer;
use Illuminate\Http\Request;

class RetailerController extends Controller
{
    public function index()
    {
        $retailers = Retailer::all();
        $states = State::all();

        return view('public.retailers.index', compact('retailers', 'states'));
    }

    public function getRetailers(Request $request)
    {   
        $retailers = Retailer::whereGeneral(boolval($request->general))->get();

        return $retailers;
    }
    public function getProductRetailers(Product $product, Request $request)
    {   
        if($request->state){
            $state = State::where('name', 'like', '%' . $request->state . '%')->first();
            $productRetailers = ProductRetailer::whereProductId($product->id)->whereStateId($state->id)->with('retailer')->get();
        }else{
            $productRetailers = null;
        }

        return $productRetailers;
    }

    public function getStates()
    {   
        $states = State::all();

        return $states;
    }

    public function getStateRetailers(Request $request)
    {   
        if($request->state){
            $state = State::where('name', 'like', '%' . $request->state . '%')->first();
            $productRetailers = StateRetailer::whereStateId($state->id)->with('retailer')->get();
        }else{
            $productRetailers = null;
        }

        return $productRetailers;
    }
    
}

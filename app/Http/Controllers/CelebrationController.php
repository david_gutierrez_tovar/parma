<?php

namespace App\Http\Controllers;

use App\Celebration;
use App\Playlist;
use Illuminate\Http\Request;

class CelebrationController extends Controller
{

    // public function index()
    // {
    //     $celebrations = Celebration::all();

    //     return view('public.experience.index', compact('celebrations'));
    // }

    public function getPlaylists()
    {   
        $playlists = Playlist::inRandomOrder()->limit(3)->get();

        return $playlists;
    }
    
    public function getCelebrations()
    {   
        $celebrations = Celebration::all();

        return $celebrations;
    }
    
    public function getCelebration(Celebration $celebration)
    {   
        // $products = Product::whereCategoryId($category->id)->get();

        return $celebration;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'presentation', 
        'name_secondary', 
        'slug',
        'about_title',
        'about',
        'legal',
        'creation',
        'creation_item1_title',
        'creation_item1_content',
        'creation_item1_image',
        'creation_item2_title',
        'creation_item2_content',
        'creation_item2_image',
        'creation_item3_title',
        'creation_item3_content',
        'creation_item3_image',
        'image_home',
        'image_products',
        'image_bg',
        'image_stamp'
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function category()
    {
        return $this->hasMany('App\Category');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('presentation');
            $table->string('name_secondary');
            $table->string('slug')->unique();
            $table->string('about_title');
            $table->text('about')->nullable();
            $table->string('legal')->nullable();
            $table->text('creation')->nullable();

            $table->string('creation_item1_title')->nullable();
            $table->text('creation_item1_content')->nullable();
            $table->string('creation_item1_image')->nullable();

            $table->string('creation_item2_title')->nullable();
            $table->text('creation_item2_content')->nullable();
            $table->string('creation_item2_image')->nullable();

            $table->string('creation_item3_title')->nullable();
            $table->text('creation_item3_content')->nullable();
            $table->string('creation_item3_image')->nullable();

            $table->string('image_home')->nullable();
            $table->string('image_products')->nullable();
            $table->string('image_bg')->nullable();
            $table->string('image_stamp')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

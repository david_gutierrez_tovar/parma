<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('full_name');
            $table->string('slug')->unique();
            $table->string('difficulty')->nullable();
            $table->string('time')->nullable();
            $table->text('introduction')->nullable();
            $table->text('ingredients')->nullable();
            $table->text('process')->nullable();
            $table->text('tip')->nullable();
            $table->string('image_thumb')->nullable();
            $table->string('image_main')->nullable();

            $table->string('video_link')->nullable();

            $table->text('ingredients_link')->nullable();
            $table->text('recipe_pdf')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}

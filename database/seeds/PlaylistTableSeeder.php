<?php

use App\Playlist;
use Illuminate\Database\Seeder;

class PlaylistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Playlist::create([
            'name' => 'Good vibes',
            'image' => 'experience/playlists/good_vibes.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DWYBO1MoTDhZI'
        ]);

        Playlist::create([
            'name' => 'Cocina de clásicos',
            'image' => 'experience/playlists/cocina_clasicos.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DWZypDDXfbyfI'
        ]);

        Playlist::create([
            'name' => 'Happy Jazz',
            'image' => 'experience/playlists/happy_jazz.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DX5YTAi6JhwZm'
        ]);

        Playlist::create([
            'name' => 'Feel good dinner',
            'image' => 'experience/playlists/feel_good_dinner.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DXbm6HfkbMtFZ'
        ]);

        Playlist::create([
            'name' => 'Feel good classics',
            'image' => 'experience/playlists/feel_good_classics.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DWVinJBuv0P4z'
        ]);

        Playlist::create([
            'name' => 'Fiesta 80era',
            'image' => 'experience/playlists/fiesta_80ra.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DWVA4ETicgIky'
        ]);

        Playlist::create([
            'name' => 'The bachelor party',
            'image' => 'experience/playlists/the_bachelor_party.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DX2pto11EMGQc'
        ]);

        Playlist::create([
            'name' => 'Punto tropical',
            'image' => 'experience/playlists/punto_tropical.jpg',
            'link' => 'https://open.spotify.com/playlist/37i9dQZF1DWZ1FUX6sM67g'
        ]);
        
    }
}

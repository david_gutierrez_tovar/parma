<?php

use App\AboutItem;
use Illuminate\Database\Seeder;

class AboutItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AboutItem::create([
            'age' => '1865',
            'description' => 'Nuestra marca se originó en la región de los Pirineos Catalanes. Desde el comienzo nos ha definido la calidad de nuestros deliciosos Madurados, así como la gama de olores y sabores en cada producto. Los cuales acompañarán tus grandes momentos familiares y amistosos con más de 55 años de tradición y experiencia en cocina europea en cada bocado y mesa que se sirvan.',
            'image' => 'somos/somos_1865.png'
        ]);

        AboutItem::create([
            'age' => '1965',
            'description' => 'Decidimos llegar a México para conquistar nuevos paladares con los sabores únicos que nos caracterizan. Nuestra experiencia nos llevó a ampliar la carta de productos y experiencias sensoriales que teníamos, ofreciendo Jamones y Salchichas. Además de los clásicos madurados que nos distinguen en el mercado internacional.',
            'image' => 'somos/somos_1965.png'
        ]);

        AboutItem::create([
            'age' => '2002',
            'description' => 'Grupo Bafar adquirió Parma, llevándola a nuevos y modernos horizontes degustativos a través de exquisitos e icónicos productos como el Queso Parmesano y el Salami Génova, pero conservando y concentrando en cada producto nuestro origen e historia, la pasión, experiencia y calidad de la cocina Española e Italiana que nos identifican.',
            'image' => 'somos/somos_2002.png'
        ]);

        AboutItem::create([
            'age' => '2008',
            'description' => 'La búsqueda de nuevos retos nos llevó a reinventar los hot dogs en México, agregándoles el toque Provoker de Parma con las mejores Salchichas Ahumadas estadounidenses y su característico Big Taste Grill; que dicho sea de pasó, llegaron para quedarse y continuar enamorando a los paladares mexicanos.',
            'image' => 'somos/somos_2008.png'
        ]);

        AboutItem::create([
            'age' => 'Actual',
            'description' => 'Actualidad: Somos reconocidos como una de las marcas más importantes de Madurados y Delicatessen en México. Hoy día nos respaldan más de 65 años de experiencia y tradición que expresamos en un extenso portafolio de productos que ofrecen un viaje de sabores, olores, texturas y experiencias únicas para todos aquellos apasionados de la cocina que buscan deleitar su paladar.',
            'image' => 'somos/somos_actual.png'
        ]);
    }
}

<?php

use App\Ingredient;
use Illuminate\Database\Seeder;

class IngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        Ingredient::create([
            'name'    => 'Aceitunas Negras',
            'image'   => 'ingredients/aceitunas-negras.png',
        ]);
        Ingredient::create([
            'name'    => 'Berenjena',
            'image'   => 'ingredients/berenjena.png',
        ]);
        Ingredient::create([
            'name'    => 'Pan Rústico de Ajo',
            'image'   => 'ingredients/pan-rustico-ajo.png',
        ]);

        // 2
        Ingredient::create([
            'name'    => 'Brócoli',
            'image'   => 'ingredients/brocoli.png',
        ]);
        Ingredient::create([
            'name'    => 'Jitomate Cherry',
            'image'   => 'ingredients/jitomate-cherry.png',
        ]);
        Ingredient::create([
            'name'    => 'Pasta Fusilli',
            'image'   => 'ingredients/pasta-fusilli.png',
        ]);

        // 3
        Ingredient::create([
            'name'    => 'Hierbabuena',
            'image'   => 'ingredients/hierbabuena.png',
        ]);
        Ingredient::create([
            'name'    => 'Pan Artesanal',
            'image'   => 'ingredients/pan-artesanal.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Camembert',
            'image'   => 'ingredients/queso-camembert.png',
        ]);

        // 4
        Ingredient::create([
            'name'    => 'Pimiento Piquillo',
            'image'   => 'ingredients/hierbabuena.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Panela',
            'image'   => 'ingredients/queso-panela.png',
        ]);
        Ingredient::create([
            'name'    => 'Rib Eye',
            'image'   => 'ingredients/rib-eye.png',
        ]);

        // 5
        Ingredient::create([
            'name'    => 'Aguacate',
            'image'   => 'ingredients/aguacate.png',
        ]);
        Ingredient::create([
            'name'    => 'Espárragos',
            'image'   => 'ingredients/esparragos.png',
        ]);
        Ingredient::create([
            'name'    => 'Lechuga Francesa',
            'image'   => 'ingredients/lechuga-francesa.png',
        ]);

        // 6
        Ingredient::create([
            'name'    => 'Bollos para Hamburguesa',
            'image'   => 'ingredients/bollos-para-hamburguesa.png',
        ]);
        Ingredient::create([
            'name'    => 'Carne Molida',
            'image'   => 'ingredients/carne-molida.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Gouda',
            'image'   => 'ingredients/queso-gouda.png',
        ]);

        // 7
        Ingredient::create([
            'name'    => 'Aceitunas Verdes',
            'image'   => 'ingredients/aceitunas-verdes.png',
        ]);
        Ingredient::create([
            'name'    => 'Hojaldras',
            'image'   => 'ingredients/hojaldras.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Mozzarella',
            'image'   => 'ingredients/queso-mozzarella.png',
        ]);

        // 8
        Ingredient::create([
            'name'    => 'Tortillas de Harina',
            'image'   => 'ingredients/tortillas-de-harina.png',
        ]);

        // 9
        Ingredient::create([
            'name'    => 'Queso de Cabra',
            'image'   => 'ingredients/queso-de-cabra.png',
        ]);
        Ingredient::create([
            'name'    => 'Tomate Verde',
            'image'   => 'ingredients/tomate-verde.png',
        ]);
        Ingredient::create([
            'name'    => 'Totopos Horneados',
            'image'   => 'ingredients/totopos-horneados.png',
        ]);

        // 10
        Ingredient::create([
            'name'    => 'Mostaza Miel',
            'image'   => 'ingredients/mostaza-miel.png',
        ]);
        Ingredient::create([
            'name'    => 'Palitos de Brocheta',
            'image'   => 'ingredients/palitos-de-brocheta.png',
        ]);
        Ingredient::create([
            'name'    => 'Piña Miel',
            'image'   => 'ingredients/pina-miel.png',
        ]);

        // 11
        Ingredient::create([
            'name'    => 'Huevo',
            'image'   => 'ingredients/huevo.png',
        ]);
        Ingredient::create([
            'name'    => 'Papa Blanca',
            'image'   => 'ingredients/papa-blanca.png',
        ]);
        Ingredient::create([
            'name'    => 'Pimiento Verde',
            'image'   => 'ingredients/pimiento-verde.png',
        ]);

        // 12
        Ingredient::create([
            'name'    => 'Cebolla Blanca',
            'image'   => 'ingredients/cebolla-blanca.png',
        ]);
        Ingredient::create([
            'name'    => 'Pan para Hot Dog',
            'image'   => 'ingredients/pan-para-hotdog.png',
        ]);
        Ingredient::create([
            'name'    => 'Tocino',
            'image'   => 'ingredients/tocino.png',
        ]);

        // 13
        Ingredient::create([
            'name'    => 'Espinacas',
            'image'   => 'ingredients/espinacas.png',
        ]);
        Ingredient::create([
            'name'    => 'Lechuga Italiana',
            'image'   => 'ingredients/lechuga-italiana.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Monterrey Jack',
            'image'   => 'ingredients/queso-monterey-jack.png',
        ]);

        // 14
        Ingredient::create([
            'name'    => 'Elote Desgranado',
            'image'   => 'ingredients/elote-desgranado.png',
        ]);
        Ingredient::create([
            'name'    => 'Pimiento Rojo',
            'image'   => 'ingredients/pimiento-rojo.png',
        ]);

        // 15
        Ingredient::create([
            'name'    => 'Bagel',
            'image'   => 'ingredients/bagel.png',
        ]);
        Ingredient::create([
            'name'    => 'Germen de Alfalfa',
            'image'   => 'ingredients/germen-de-alfalfa.png',
        ]);
        Ingredient::create([
            'name'    => 'Queso Crema',
            'image'   => 'ingredients/queso-crema.png',
        ]);

        // 16
        Ingredient::create([
            'name'    => 'Albahaca',
            'image'   => 'ingredients/albahaca.png',
        ]);
        Ingredient::create([
            'name'    => 'Pure de Tomate',
            'image'   => 'ingredients/pure-de-tomate.png',
        ]);

        // 17
        Ingredient::create([
            'name'    => 'Aceite de Oliva',
            'image'   => 'ingredients/aceite-de-oliva.png',
        ]);
        Ingredient::create([
            'name'    => 'Hongo Portobello',
            'image'   => 'ingredients/hongo-portobello.png',
        ]);

        // 18
        Ingredient::create([
            'name'    => 'Champiñones',
            'image'   => 'ingredients/champiñones.png',
        ]);
        Ingredient::create([
            'name'    => 'Mantequilla',
            'image'   => 'ingredients/mantequilla.png',
        ]);
        Ingredient::create([
            'name'    => 'Pan de Barra',
            'image'   => 'ingredients/pan-de-barra.png',
        ]);


    }
}

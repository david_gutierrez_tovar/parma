<?php

use App\RecipeProduct;
use Illuminate\Database\Seeder;

class RecipeProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RecipeProduct::create([
            'recipe_id' 	=> 1,
            'product_id'	=> 8
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 2,
            'product_id'	=> 17
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 3,
            'product_id'	=> 6
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 4,
            'product_id'	=> 7
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 5,
            'product_id'	=> 9
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 6,
            'product_id'	=> 10
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 6,
            'product_id'	=> 14
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 7,
            'product_id'	=> 16
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 8,
            'product_id'	=> 7
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 9,
            'product_id'	=> 12
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 10,
            'product_id'	=> 22
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 11,
            'product_id'	=> 10
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 12,
            'product_id'	=> 20
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 13,
            'product_id'	=> 4
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 14,
            'product_id'	=> 12
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 15,
            'product_id'	=> 4
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 16,
            'product_id'	=> 10
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 17,
            'product_id'	=> 28
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 18,
            'product_id'	=> 3
        ]);
        RecipeProduct::create([
            'recipe_id' 	=> 18,
            'product_id'	=> 25
        ]);

    }
}

<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'David',
            'email'     => 'david.gutierrez.tovar@gmail.com',
            'password'  => bcrypt('pepepepe'),
        ]);

        User::create([
            'name'      => 'Monserrat Pineda',
            'email'     => 'monserrat@radicaltesta.com',
            'password'  => bcrypt('Parma2020'),
        ]);
    }
}

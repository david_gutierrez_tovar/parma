<?php

use App\Retailer;
use Illuminate\Database\Seeder;

class RetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Retailer::create([
            'name'          => 'City Market',
            'image_white'   => 'retailers/citymarket_white.png',
            'image_black'   => 'retailers/citymarket_black.png',
            'link'          => 'https://www.lacomer.com.mx/lacomer/#!/item-search/380/parma?succId=380&succFmt=200'
        ]);
        Retailer::create([
            'name'          => 'Fresko',
            'image_white'   => 'retailers/fresko_white.png',
            'image_black'   => 'retailers/fresko_black.png',
            'link'          => 'https://www.lacomer.com.mx/lacomer/?gclid=EAIaIQobChMI4qm15P6j8AIV9AaICR2tFwuAEAAYASAAEgJV5PD_BwE#!/item-search/137/parma?succId=137&succFmt=100 '
        ]);
        Retailer::create([
            'name'          => 'La Comer',
            'image_white'   => 'retailers/lacomer_white.png',
            'image_black'   => 'retailers/lacomer_black.png',
            'link'          => 'https://www.lacomer.com.mx/lacomer/#!/item-search/235/parma?succId=235&succFmt=100'
        ]);
        Retailer::create([
            'name'          => 'Sumesa',
            'image_white'   => 'retailers/sumesa_white.png',
            'image_black'   => 'retailers/sumesa_black.png',
            'link'          => 'https://www.lacomer.com.mx/lacomer/#!/item-search/235/parma?succId=235&succFmt=100'
        ]);
        Retailer::create([
            'name'          => 'Rappi',
            'image_white'   => 'retailers/rappi_white.png',
            'image_black'   => 'retailers/rappi_black.png',
            'link'          => 'https://www.rappi.com.mx/search?store_type=market&query=parma&search_type=TYPED&origin=by_store_type ',
            'general'       => 1
        ]);
        Retailer::create([
            'name'          => 'Walmart',
            'image_white'   => 'retailers/walmart_white.png',
            'image_black'   => 'retailers/walmart_black.png',
            'link'          => 'https://super.walmart.com.mx/productos?Ntt=parma',
            'general'       => 1
        ]);
        Retailer::create([
            'name'          => 'Superama',
            'image_white'   => 'retailers/superama_white.png',
            'image_black'   => 'retailers/superama_black.png',
            'link'          => 'https://www.superama.com.mx/buscar/parma',
            'general'       => 1
        ]);
        Retailer::create([
            'name'          => 'Soriana',
            'image_white'   => 'retailers/soriana_white.png',
            'image_black'   => 'retailers/soriana_black.png',
            'link'          => 'https://superentucasa.soriana.com/default.aspx?p=13365&postback=1&Txt_Bsq_Descripcion=parma&cantCeldas=0&minCeldas=0 ',
        ]);
        Retailer::create([
            'name'          => 'alsuper',
            'image_white'   => 'retailers/alsuper_white.png',
            'image_black'   => 'retailers/alsuper_black.png',
            'link'          => 'https://www.alsuper.com/buscar?q=parma',
        ]);
        Retailer::create([
            'name'          => 'Cornershop',
            'image_white'   => 'retailers/cornershop_white.png',
            'image_black'   => 'retailers/cornershop_black.png',
            'link'          => 'https://web.cornershopapp.com/stores-search/parma',
        ]);
        Retailer::create([
            'name'          => 'HEB',
            'image_white'   => 'retailers/heb_white.png',
            'image_black'   => 'retailers/heb_black.png',
            'link'          => 'https://www.heb.com.mx/catalogsearch/result/?q=PARMA',
        ]);
        Retailer::create([
            'name'          => 'Cheadrui',
            'image_white'   => 'retailers/chedraui_white.png',
            'image_black'   => 'retailers/chedraui_black.png',
            'link'          => 'https://www.chedraui.com.mx/search?text=parma&method=enter',
        ]);
    }
}

<?php

use App\StateRetailer;
use Illuminate\Database\Seeder;

class StateRetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=EdoMex&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Aguascalientes&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=BCN&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 3,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=BCS&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=BCS&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=BCS&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=BCS&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=BCS&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Campeche&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Campeche&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Cancun&utm_content=7635'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/231/Parma?succId=231&succFmt=100&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=CDMX&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Chiahuahua&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Chiapas&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Chiapas&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParrillaParma&utm_term=Chihuahua&utm_content=9165'
        ]);
        StateRetailer::create([
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=Coahuila&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Coahuila&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParrillaParma&utm_term=Coahuila&utm_content=9165'
        ]);
        StateRetailer::create([
            'state_id'		=> 6,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Colima&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 6,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Colima&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Colima&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Colima&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Durango&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Durango&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParrillaParma&utm_term=Durango&utm_content=9165'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=EdoMex&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=EdoMex&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=EdoMex&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=EdoMex&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=EdoMexico&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term= EdoMexico&utm_content=9140'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Guanajuato&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Guerrero&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Hidalgo&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Hidalgo&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Jalisco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Michoacan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/231/Parma?succId=231&succFmt=100&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Morelos&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 18,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Nayarit&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 18,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Nayarit&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Nayarit&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Nayarit&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 18,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Nayarit&utm_content=7635'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=NvoLeon&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Oaxaca&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Oaxaca&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=9128'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Puebla&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/403/Parma?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=9171'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/423/Parma?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=9192'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/item-search/287/Parma?succId=287&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Queretaro&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Quintana Roo&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=QuintanaRoo&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=QuintanaRoo&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Sinaloa&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=SnLuisPotosi&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Sonora&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Tabasco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Tabasco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Tabasco&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/catalogsearch/result/?q=Parma?utm_source=Website&utm_medium=HEB&utm_campaign=ParrillaParma&utm_term=Tamaulipas&utm_content=9135'
        ]);
        StateRetailer::create([
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Tamaulipas&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 28,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Tamaulipas&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Tlaxcala&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 29,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Tlaxcala&utm_content=7635'
        ]);
        StateRetailer::create([
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Veracruz&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Veracruz&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Veracruz&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/buscar/parma?utm_source=Website&utm_medium=Superama&utm_campaign=ParrillaParma&utm_term=Yucatan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Yucatan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Yucatan&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/productos?Ntt=Parma?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParrillaParma&utm_term=Zacatecas&utm_content=7634'
        ]);
        StateRetailer::create([
            'state_id'		=> 32,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/search?text=Parma&method=enter&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParrillaParma&utm_term=Zacatecas&utm_content=7635'
        ]);

    }
}

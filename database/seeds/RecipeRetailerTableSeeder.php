<?php

use App\RecipeRetailer;
use Illuminate\Database\Seeder;

class RecipeRetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // WALMART
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/torta-de-jamon/_/cat3780086?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_torta_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 1,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/fusilli/_/cat3780088?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_fusilli_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 2,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/bruschettas/_/cat3780089?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_bruschettas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 3,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/tacos/_/cat3780090?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_tacos_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 4,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/ensalada/_/cat3780091?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_ensalada_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 5,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/hamburguesa/_/cat3780092?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_hamburguesa_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 6,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/tartita/_/cat3780093?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_tartita_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 7,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/sincronizadas/_/cat3780094?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_sincronizadas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 8,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.lacomer.com.mx/lacomer/?gclid=CjwKCAjwmK6IBhBqEiwAocMc8vzzI7OkmAkekxlots330XjtLF3yWRxE3NE7tEv2k8PLGG7ZHraWWxoCE0AQAvD_BwE#!/detarticulo/7501518491265/0/807/1///807&succId=287&succFmt=100?utm_source=trd_bafar&utm_medium=trd_parmamx&utm_campaign=trd_bafar_mf_receta_chilaquiles_20210806&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 9,
            'retailer_id'	=> 3
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/brochetas/_/cat3780095?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_brochetas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 10,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/huevos/_/cat3780096?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_huevos_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 11,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/hot-dog/_/cat3780097?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_hot-dog_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 12,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/wrap/_/cat3780098?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_wrap_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 13,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/carnes-frias/chorizo-tipo-argentino-parma-400-g/00750151849161?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_empanada_20210806&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 14,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/bagel/_/cat3780099?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_bagel_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 15,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/fusilli-salsa/_/cat3780100?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_fusilli-salsa_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 16,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/promociones-especiales/bafar/portobello/_/cat3780101?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_portobello_20210723&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 17,
            'retailer_id'	=> 6
        ]);
        RecipeRetailer::create([
            'link'          => 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_croque_20210806&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 18,
            'retailer_id'	=> 6
        ]);


        // SUPERAMA
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-torta?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 1,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-fusilli?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 2,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => ' https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-brus?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 3,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-tacos?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 4,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-salads?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 5,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-hambu?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 6,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-tarti?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 7,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-sincronizadas?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 8,
            'retailer_id'	=> 7
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 9,
        //     'retailer_id'	=> 7
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-brochetas?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 10,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-huevos?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 11,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-hot?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 12,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-wrap?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 13,
            'retailer_id'	=> 7
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 14,
        //     'retailer_id'	=> 7
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-bagel?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 15,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-fus?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 16,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-portobello?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 17,
            'retailer_id'	=> 7
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.superama.com.mx/catalogo/c-c100-marcas/cf-c100-bafar/cl-c100-croque?utm_source=jbm_parma&utm_medium=jbm_parma_sitio&utm_campaign=jbm_parma_mf_recetas_20210915&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 18,
            'retailer_id'	=> 7
        ]);


        // HEB
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/torta-de-jamon-con-berenjena?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 1,
            'retailer_id'	=> 11
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 2,
        //     'retailer_id'	=> 11
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/bruschettas-de-jamon-serrano?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 3,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/tacos-de-ribeye-y-jamon-serrano?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 4,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/ensalada-de-aguacate-con-crujiente-serrano?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 5,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/hamburguesa-de-chistorra-y-salami?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 6,
            'retailer_id'	=> 11
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 7,
        //     'retailer_id'	=> 11
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/sincronizadas-con-jamon-serrano?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 8,
            'retailer_id'	=> 11
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 9,
        //     'retailer_id'	=> 3
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/brochetas-de-salchicha-y-pina?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 10,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/huevos-con-chistorra-y-setas?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 11,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/hotdog-de-salchicha-cheddar?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 12,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/wrap-de-pechuga-de-pavo-y-queso?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 13,
            'retailer_id'	=> 11
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 14,
        //     'retailer_id'	=> 11
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/bagel-de-pechuga-de-pavo?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 15,
            'retailer_id'	=> 11
        ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/fusilli-con-salsa-de-chistorra-y-tomate?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 16,
            'retailer_id'	=> 11
        ]);
        // RecipeRetailer::create([
        //     'link'          => '',
        //     'recipe_id' 	=> 17,
        //     'retailer_id'	=> 11
        // ]);
        RecipeRetailer::create([
            'link'          => 'https://www.heb.com.mx/promociones/parma/croque-monsieur?utm_source=trd_parma&utm_medium=trd_sitio_parma&utm_campaign=trd_parma_mf_indefinido_20211008&utm_content=referral&utm_term=fijo',
            'recipe_id' 	=> 18,
            'retailer_id'	=> 11
        ]);

    }
}

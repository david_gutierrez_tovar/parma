<?php

use App\RecipeIngredient;
use Illuminate\Database\Seeder;

class RecipeIngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        RecipeIngredient::create([
            'recipe_id' 	=> 1,
            'ingredient_id'	=> 1
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 1,
            'ingredient_id'	=> 2
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 1,
            'ingredient_id'	=> 3
        ]);

        //2
        RecipeIngredient::create([
            'recipe_id' 	=> 2,
            'ingredient_id'	=> 4
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 2,
            'ingredient_id'	=> 5
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 2,
            'ingredient_id'	=> 6
        ]);

        //3
        RecipeIngredient::create([
            'recipe_id' 	=> 3,
            'ingredient_id'	=> 7
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 3,
            'ingredient_id'	=> 8
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 3,
            'ingredient_id'	=> 9
        ]);

        //4
        RecipeIngredient::create([
            'recipe_id' 	=> 4,
            'ingredient_id'	=> 10
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 4,
            'ingredient_id'	=> 11
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 4,
            'ingredient_id'	=> 12
        ]);

        //5
        RecipeIngredient::create([
            'recipe_id' 	=> 5,
            'ingredient_id'	=> 13
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 5,
            'ingredient_id'	=> 14
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 5,
            'ingredient_id'	=> 15
        ]);

        //6
        RecipeIngredient::create([
            'recipe_id' 	=> 6,
            'ingredient_id'	=> 16
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 6,
            'ingredient_id'	=> 17
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 6,
            'ingredient_id'	=> 18
        ]);

        //7
        RecipeIngredient::create([
            'recipe_id' 	=> 7,
            'ingredient_id'	=> 19
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 7,
            'ingredient_id'	=> 20
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 7,
            'ingredient_id'	=> 21
        ]);

        //8
        RecipeIngredient::create([
            'recipe_id' 	=> 8,
            'ingredient_id'	=> 13
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 8,
            'ingredient_id'	=> 18
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 8,
            'ingredient_id'	=> 22
        ]);

        //9
        RecipeIngredient::create([
            'recipe_id' 	=> 9,
            'ingredient_id'	=> 23
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 9,
            'ingredient_id'	=> 24
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 9,
            'ingredient_id'	=> 25
        ]);

        //10
        RecipeIngredient::create([
            'recipe_id' 	=> 10,
            'ingredient_id'	=> 26
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 10,
            'ingredient_id'	=> 27
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 10,
            'ingredient_id'	=> 28
        ]);

        //11
        RecipeIngredient::create([
            'recipe_id' 	=> 11,
            'ingredient_id'	=> 29
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 11,
            'ingredient_id'	=> 30
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 11,
            'ingredient_id'	=> 31
        ]);

        //12
        RecipeIngredient::create([
            'recipe_id' 	=> 12,
            'ingredient_id'	=> 32
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 12,
            'ingredient_id'	=> 33
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 12,
            'ingredient_id'	=> 34
        ]);

        //13
        RecipeIngredient::create([
            'recipe_id' 	=> 13,
            'ingredient_id'	=> 35
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 13,
            'ingredient_id'	=> 36
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 13,
            'ingredient_id'	=> 37
        ]);

        //14
        RecipeIngredient::create([
            'recipe_id' 	=> 14,
            'ingredient_id'	=> 38
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 14,
            'ingredient_id'	=> 32
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 14,
            'ingredient_id'	=> 39
        ]);

        //15
        RecipeIngredient::create([
            'recipe_id' 	=> 15,
            'ingredient_id'	=> 40
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 15,
            'ingredient_id'	=> 41
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 15,
            'ingredient_id'	=> 42
        ]);

        //16
        RecipeIngredient::create([
            'recipe_id' 	=> 16,
            'ingredient_id'	=> 43
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 16,
            'ingredient_id'	=> 6
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 16,
            'ingredient_id'	=> 44
        ]);

        //17
        RecipeIngredient::create([
            'recipe_id' 	=> 17,
            'ingredient_id'	=> 45
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 17,
            'ingredient_id'	=> 40
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 17,
            'ingredient_id'	=> 46
        ]);

        //18
        RecipeIngredient::create([
            'recipe_id' 	=> 18,
            'ingredient_id'	=> 47
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 18,
            'ingredient_id'	=> 48
        ]);
        RecipeIngredient::create([
            'recipe_id' 	=> 18,
            'ingredient_id'	=> 49
        ]);
        

    }
}

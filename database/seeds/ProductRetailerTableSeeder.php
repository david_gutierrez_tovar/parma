<?php

use App\ProductRetailer;
use Illuminate\Database\Seeder;

class ProductRetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=EdoMex&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=Puebla&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Chistorra350g&utm_term=NvoLeon&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=EdoMex&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Chistorra350g&utm_term=BCS&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=EdoMex&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Colima&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=BCS&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Nayarit&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Puebla&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Chistorra350g&utm_term=Aguascalientes&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=EdoMex&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=CDMX&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=Queretaro&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=Jalisco&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=Puebla&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=Morelos&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ChorizoArgentino400g&utm_term=NvoLeon&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=EdoMex&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=CDMX&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=Queretaro&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=Guanajuato&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=Jalisco&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=Morelos&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ChorizoArgentino400g&utm_term=BCS&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Queretaro&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Guanajuato&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=EdoMex&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=CDMX&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Colima&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=BCS&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Morelos&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Nayarit&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Jalisco&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Puebla&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491616/0/801/1///801?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ChorizoArgentino400g&utm_term=Aguascalientes&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=EdoMex&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=Queretaro&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=Jalisco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=Puebla&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano120g&utm_term=NvoLeon&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=EdoMex&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=Queretaro&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=Guanajuato&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=Jalisco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano120g&utm_term=BCS&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=EdoMex&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Colima&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Queretaro&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=BCS&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Guanajuato&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Nayarit&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Jalisco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Puebla&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano120g&utm_term=Aguascalientes&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=EdoMex&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=CDMX&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=Queretaro&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=Jalisco&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=Puebla&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=Morelos&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SerranoPorKg &utm_term=NvoLeon&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=EdoMex&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=CDMX&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=Queretaro&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=Guanajuato&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=Jalisco&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=Morelos&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SerranoPorKg &utm_term=BCS&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Guanajuato&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=EdoMex&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=CDMX&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Colima&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Queretaro&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=BCS&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Morelos&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Nayarit&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Jalisco&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Puebla&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SerranoPorKg &utm_term=Aguascalientes&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=EdoMex&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=Jalisco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=Puebla&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Serrano100g&utm_term=NvoLeon&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=EdoMex&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=Guanajuato&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=Jalisco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Serrano100g&utm_term=BCS&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Guanajuato&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=EdoMex&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Michoacan&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Colima&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=BCS&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Guerrero&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Nayarit&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Jalisco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Puebla&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Serrano100g&utm_term=Aguascalientes&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=EdoMex&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=Puebla&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=CharolaSerrano150g&utm_term=NvoLeon&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=EdoMex&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=CharolaSerrano150g&utm_term=BCS&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=EdoMex&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Michoacan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Colima&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=BCS&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Guerrero&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Nayarit&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Puebla&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=CharolaSerrano150g&utm_term=Aguascalientes&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Puebla&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=NvoLeon&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Guanajuato&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=BCS&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Colima&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=BCS&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Guanajuato&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Nayarit&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Puebla&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Aguascalientes&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=EdoMex&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=CDMX&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=Jalisco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=Puebla&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=Morelos&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=York200g&utm_term=NvoLeon&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=EdoMex&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=CDMX&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=Guanajuato&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=Jalisco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=Morelos&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=York200g&utm_term=BCS&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=EdoMex&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=CDMX&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Colima&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=BCS&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Morelos&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Guanajuato&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Nayarit&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Jalisco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Puebla&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491586/0/804/1///804?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=York200g&utm_term=Aguascalientes&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Queretaro&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Guanajuato&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=EdoMex&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Michoacan&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=CDMX&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Colima&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=BCS&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Morelos&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Nayarit&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Jalisco&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Puebla&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491159/0/835/1///835?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=LaminasParmesano140g&utm_term=Aguascalientes&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=EdoMex&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=CDMX&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=Queretaro&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=Jalisco&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=Puebla&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=Morelos&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=MiniSalchichas400g&utm_term=NvoLeon&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=EdoMex&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=CDMX&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=Queretaro&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=Guanajuato&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=Jalisco&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=Morelos&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=402&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=MiniSalchichas400g&utm_term=BCS&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Queretaro&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Guanajuato&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=EdoMex&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=CDMX&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Michoacan&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Colima&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=BCS&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Morelos&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Guerrero&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Nayarit&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Jalisco&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Puebla&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=MiniSalchichas400g&utm_term=Aguascalientes&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=EdoMex&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=Puebla&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pechuga180g&utm_term=NvoLeon&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=EdoMex&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pechuga180g&utm_term=BCS&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=EdoMex&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Colima&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=BCS&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Nayarit&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Puebla&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2623235000003/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pechuga180g&utm_term=Aguascalientes&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Puebla&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=379&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=NvoLeon&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Guanajuato&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=BCS&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Colima&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Queretaro&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=BCS&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Guanajuato&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Nayarit&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Jalisco&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Puebla&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Aguascalientes&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=EdoMex&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=Queretaro&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=Puebla&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=408&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Pepperoni100g&utm_term=NvoLeon&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=EdoMex&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=Queretaro&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=Guanajuato&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Pepperoni100g&utm_term=BCS&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Queretaro&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Guanajuato&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=EdoMex&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Michoacan&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Colima&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=BCS&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Guerrero&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Nayarit&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Puebla&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Pepperoni100g&utm_term=Aguascalientes&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=EdoMex&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=Puebla&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido227g&utm_term=NvoLeon&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=EdoMex&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=Guanajuato&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido227g&utm_term=BCS&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Guanajuato&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=EdoMex&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Michoacan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Colima&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=BCS&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Guerrero&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Nayarit&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Puebla&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido227g&utm_term=Aguascalientes&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=EdoMex&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=Queretaro&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=Jalisco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=Puebla&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=ParmesanoMolido85g&utm_term=NvoLeon&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=EdoMex&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=Queretaro&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=Guanajuato&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=Jalisco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=ParmesanoMolido85g&utm_term=BCS&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Queretaro&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Guanajuato&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=EdoMex&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Michoacan&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Colima&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=BCS&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Guerrero&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Nayarit&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Jalisco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Puebla&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=ParmesanoMolido85g&utm_term=Aguascalientes&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=EdoMex&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=CDMX&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=Queretaro&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=Jalisco&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=Puebla&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=Morelos&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalamiPorKg&utm_term=NvoLeon&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=EdoMex&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=CDMX&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=Queretaro&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=Guanajuato&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=Jalisco&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=Morelos&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalamiPorKg&utm_term=BCS&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Queretaro&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=EdoMex&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=CDMX&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Colima&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=BCS&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Morelos&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Guanajuato&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Nayarit&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Jalisco&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Puebla&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalamiPorKg&utm_term=Aguascalientes&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=EdoMex&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=Puebla&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=Salami100g&utm_term=NvoLeon&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=EdoMex&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=Salami100g&utm_term=BCS&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=EdoMex&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Michoacan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Colima&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=BCS&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Guerrero&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Nayarit&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Puebla&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=Salami100g&utm_term=Aguascalientes&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMex&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Jalisco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Puebla&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=NvoLeon&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMex&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guanajuato&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Jalisco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=423&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=BCS&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guanajuato&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMex&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Colima&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=BCS&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Nayarit&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Jalisco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Puebla&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810?succId=420&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Aguascalientes&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=EdoMex&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=CDMX&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=Queretaro&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=Jalisco&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=Puebla&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=Morelos&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=419&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaRes350g&utm_term=NvoLeon&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=EdoMex&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=CDMX&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=Queretaro&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=Guanajuato&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=Jalisco&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=Morelos&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaRes350g&utm_term=BCS&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Queretaro&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Guanajuato&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=EdoMex&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=CDMX&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=BCS&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Morelos&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Nayarit&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Jalisco&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Puebla&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaRes350g&utm_term=Aguascalientes&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=EdoMex&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=CDMX&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=Queretaro&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=Jalisco&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=Puebla&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=Morelos&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaAhumada397g&utm_term=NvoLeon&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=EdoMex&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=CDMX&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=Queretaro&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=Guanajuato&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=Jalisco&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=Morelos&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaAhumada397g&utm_term=BCS&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Queretaro&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Guanajuato&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=EdoMex&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=CDMX&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Michoacan&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Colima&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=BCS&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Morelos&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 12,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Guerrero&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Nayarit&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Jalisco&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Puebla&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaAhumada397g&utm_term=Aguascalientes&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=EdoMex&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=Jalisco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=Puebla&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=377&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SalchichaCheddar397g&utm_term=NvoLeon&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=EdoMex&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=Guanajuato&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=Jalisco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SalchichaCheddar397g&utm_term=BCS&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=EdoMex&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Colima&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=BCS&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Guanajuato&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Nayarit&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Jalisco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Puebla&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SalchichaCheddar397g&utm_term=Aguascalientes&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=EdoMex&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=Puebla&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=SeleccionGourmet150g&utm_term=NvoLeon&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=EdoMex&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=Guanajuato&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=SeleccionGourmet150g&utm_term=BCS&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=EdoMex&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Colima&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=BCS&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Guanajuato&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Nayarit&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Puebla&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=SeleccionGourmet150g&utm_term=Aguascalientes&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 15,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=EdoMex&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 9,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=CDMX&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 22,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=Queretaro&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 14,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=Jalisco&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 21,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=Puebla&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 17,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=Morelos&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 19,
            'retailer_id'	=> 1,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=403&succFmt=200&utm_source=Website&utm_medium=City Market&utm_campaign=TresQuesos227g&utm_term=NvoLeon&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 15,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=EdoMex&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 9,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=CDMX&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 22,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=Queretaro&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 11,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=Guanajuato&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 14,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=Jalisco&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 17,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=Morelos&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 3,
            'retailer_id'	=> 2,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=426&succFmt=100&utm_source=Website&utm_medium=Fresko&utm_campaign=TresQuesos227g&utm_term=BCS&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 22,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Queretaro&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 11,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Guanajuato&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 15,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=EdoMex&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 9,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=CDMX&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 16,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Michoacan&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 6,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Colima&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 3,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=BCS&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 17,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Morelos&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 18,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Nayarit&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 14,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Jalisco&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 21,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Puebla&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 1,
            'retailer_id'	=> 3,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34?succId=398&succFmt=100&utm_source=Website&utm_medium=La Comer&utm_campaign=TresQuesos227g&utm_term=Aguascalientes&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801&utm_source=Website&utm_medium=Sumesa&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491715/0/801/1///801&utm_source=Website&utm_medium=Sumesa&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491364/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=SerranoPorKg &utm_term=CDMX&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622207000003/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=SerranoPorKg &utm_term=Morelos&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491357/0/812/1///812&utm_source=Website&utm_medium=Sumesa&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813&utm_source=Website&utm_medium=Sumesa&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491654/0/813/1///813&utm_source=Website&utm_medium=Sumesa&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804&utm_source=Website&utm_medium=Sumesa&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624855000008/0/804/1///804&utm_source=Website&utm_medium=Sumesa&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=MiniSalchichas400g&utm_term=CDMX&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491784/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=MiniSalchichas400g&utm_term=Morelos&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809&utm_source=Website&utm_medium=Sumesa&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2624856000007/0/809/1///809&utm_source=Website&utm_medium=Sumesa&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491289/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476347/0/837/1///837&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518476354/0/837/1///837&utm_source=Website&utm_medium=Sumesa&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalamiPorKg&utm_term=CDMX&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/2622426000006/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalamiPorKg&utm_term=Morelos&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491401/0/807/1///807&utm_source=Website&utm_medium=Sumesa&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491821/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaRes350g&utm_term=CDMX&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491883/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaRes350g&utm_term=Morelos&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaAhumada397g&utm_term=CDMX&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491807/0/810/1///810&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaAhumada397g&utm_term=Morelos&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491814/0/810/1///810?succId=252&succFmt=100&utm_source=Website&utm_medium=Sumesa&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801&utm_source=Website&utm_medium=Sumesa&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491296/0/801/1///801&utm_source=Website&utm_medium=Sumesa&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 9,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34&utm_source=Website&utm_medium=Sumesa&utm_campaign=TresQuesos227g&utm_term=CDMX&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 17,
            'retailer_id'	=> 4,
            'link'			=> 'https://www.lacomer.com.mx/lacomer/#!/detarticulo/7501518491098/0/34/1///34&utm_source=Website&utm_medium=Sumesa&utm_campaign=TresQuesos227g&utm_term=Morelos&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=NvoLeon&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=Coahuila&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=Tamaulipas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=Guanajuato&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=Aguascalientes&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-serrano-alto-vacio-100-gr-138380.html?utm_source=Website&utm_medium=HEB&utm_campaign=Serrano100g&utm_term=SnLuisPotosi&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=NvoLeon&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=Coahuila&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=Tamaulipas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=Aguascalientes&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-salami-salami-genova-100-gr-144634.html?utm_source=Website&utm_medium=HEB&utm_campaign=Salami100g&utm_term=SnLuisPotosi&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=NvoLeon&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=Coahuila&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=Tamaulipas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=Aguascalientes&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-delicharola-jamon-serrano-150-gr-171731.html?utm_source=Website&utm_medium=HEB&utm_campaign=CharolaSerrano150g&utm_term=SnLuisPotosi&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=NvoLeon&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=Coahuila&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=Tamaulipas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=Aguascalientes&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-chistorra-chistorra-parma-350-gr-323807.html?utm_source=Website&utm_medium=HEB&utm_campaign=Chistorra350g&utm_term=SnLuisPotosi&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-parma-chorizo-sarta-250-gr-607664.html?utm_source=Website&utm_medium=HEB&utm_campaign=ChorizoSarta250g&utm_term=NvoLeon&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-parma-chorizo-sarta-250-gr-607664.html?utm_source=Website&utm_medium=HEB&utm_campaign=ChorizoSarta250g&utm_term=Coahuila&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-parma-chorizo-sarta-250-gr-607664.html?utm_source=Website&utm_medium=HEB&utm_campaign=ChorizoSarta250g&utm_term=Guanajuato&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/super/carnes-pescados-salchichoneria/carnes-frias/parma-parma-chorizo-sarta-250-gr-607664.html?utm_source=Website&utm_medium=HEB&utm_campaign=ChorizoSarta250g&utm_term=Queretaro&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=NvoLeon&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=Coahuila&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=Tamaulipas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=Aguascalientes&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-pechuga-pavo-parma-180-gr-pechuga-pavo-parma-180-0-18-gr-727590.html?utm_source=Website&utm_medium=HEB&utm_campaign=Pechuga180g&utm_term=SnLuisPotosi&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 19,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=NvoLeon&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 5,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=Coahuila&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 28,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=Tamaulipas&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 11,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=Guanajuato&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 1,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=Aguascalientes&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 24,
            'retailer_id'	=> 11,
            'link'			=> 'https://www.heb.com.mx/parma-jamon-pierna-york-parma-200-gr-jamon-pierna-york-0-2-gr-727591.html?utm_source=Website&utm_medium=HEB&utm_campaign=York200g&utm_term=SnLuisPotosi&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Aguascalientes&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=EdoMexico&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Guanajuato&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Guerrero&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Michoacan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=NvoLeon&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Puebla&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=QuintanaRoo&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Tabasco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Veracruz&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-lacteos-y-huevo/f-quesos/l-parmesano/queso-parmesano-parma-227-g/0750151847634?utm_source=Website&utm_medium=Superama&utm_campaign=ParmesanoMolido227g&utm_term=Yucatan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=EdoMexico&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Aguascalientes&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=EdoMexico&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Guerrero&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Michoacan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=NvoLeon&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Puebla&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=QuintanaRoo&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=SnLuisPotosi&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Tabasco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Veracruz&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/salami-parma-tipo-genova-100-g/0750151849140?utm_source=Website&utm_medium=Superama&utm_campaign=Salami100g&utm_term=Yucatan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=EdoMexico&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Aguascalientes&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=EdoMexico&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Guerrero&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Michoacan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=NvoLeon&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Puebla&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=QuintanaRoo&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=SnLuisPotosi&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Tabasco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Veracruz&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-charola-delicatessen/jamon-serrano-parma-charola-150-g/0750151849165?utm_source=Website&utm_medium=Superama&utm_campaign=CharolaSerrano150g&utm_term=Yucatan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term= EdoMexico&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Aguascalientes&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=EdoMexico&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Guerrero&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Michoacan&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=NvoLeon&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Puebla&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=QuintanaRoo&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=SnLuisPotosi&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Tabasco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Veracruz&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-chorizo-y-longaniza/chistorra-parma-350-g/0750151849171?utm_source=Website&utm_medium=Superama&utm_campaign=Chistorra350g&utm_term=Yucatan&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term= EdoMexico&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Aguascalientes&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=EdoMexico&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Guanajuato&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Guerrero&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Jalisco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Michoacan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=NvoLeon&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Puebla&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=QuintanaRoo&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=SnLuisPotosi&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Tabasco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Veracruz&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-397-g/0750151849181?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddar397g&utm_term=Yucatan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term= EdoMexico&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Aguascalientes&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMexico&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guanajuato&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guerrero&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Jalisco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Michoacan&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=NvoLeon&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Puebla&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=QuintanaRoo&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=SnLuisPotosi&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Tabasco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Veracruz&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-tocino/salchicha-parma-con-cheddar-y-jalapeno-397-g/0750151849182?utm_source=Website&utm_medium=Superama&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Yucatan&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term= EdoMexico&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Aguascalientes&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=EdoMexico&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Guanajuato&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Guerrero&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Jalisco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Michoacan&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=NvoLeon&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Puebla&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Queretaro&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=QuintanaRoo&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=SnLuisPotosi&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Tabasco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Veracruz&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-delicatessen/l-jamon-serrano/jamon-serrano-parma-120-g/0750151849136?utm_source=Website&utm_medium=Superama&utm_campaign=Serrano120g&utm_term=Yucatan&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term= EdoMexico&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Aguascalientes&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=EdoMexico&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Guanajuato&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Guerrero&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Michoacan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=NvoLeon&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Puebla&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=QuintanaRoo&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=SnLuisPotosi&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Tabasco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Veracruz&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/0750151849129?utm_source=Website&utm_medium=Superama&utm_campaign=SeleccionGourmet150g&utm_term=Yucatan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/pepperoni-parma-100-g/0750151849128?utm_source=Website&utm_medium=Superama&utm_campaign=Pepperoni100g&utm_term=Aguascalientes&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/pepperoni-parma-100-g/0750151849128?utm_source=Website&utm_medium=Superama&utm_campaign=Pepperoni100g&utm_term=Guanajuato&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/pepperoni-parma-100-g/0750151849128?utm_source=Website&utm_medium=Superama&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-salami/pepperoni-parma-100-g/0750151849128?utm_source=Website&utm_medium=Superama&utm_campaign=Pepperoni100g&utm_term=Veracruz&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term= EdoMexico&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Aguascalientes&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=CDMX&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=EdoMexico&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Guanajuato&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Guerrero&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Jalisco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Morelos&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=NvoLeon&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Puebla&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=QuintanaRoo&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=SnLuisPotosi&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Veracruz&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-jamon/jamon-de-pierna-parma-york-200-g/0750151849158?utm_source=Website&utm_medium=Superama&utm_campaign=York200g&utm_term=Yucatan&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term= EdoMexico&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 1,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Aguascalientes&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=EdoMexico&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 12,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Guerrero&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 16,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Michoacan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 19,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=NvoLeon&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 21,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Puebla&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 23,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=QuintanaRoo&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 24,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=SnLuisPotosi&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 27,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Tabasco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 30,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Veracruz&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 31,
            'retailer_id'	=> 7,
            'link'			=> 'https://www.superama.com.mx/catalogo/d-salchichoneria-y-gourmet/f-carnes-frias/l-pechuga-de-pavo/pechuga-de-pavo-parma-180-g/0750151849176?utm_source=Website&utm_medium=Superama&utm_campaign=Pechuga180g&utm_term=Yucatan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Zacatecas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Aguascalientes&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=BCN&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=BCS&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Campeche&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Chiapas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Chiahuahua&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Coahuila&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Colima&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Durango&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=EdoMex&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Guanajuato&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Guerrero&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Hidalgo&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Michoacan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Nayarit&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=NvoLeon&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Oaxaca&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Puebla&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=QuintanaRoo&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Sinaloa&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Sonora&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Tabasco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Tamaulipas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Tlaxcala&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Veracruz&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Yucatan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-227-g/00750151847634?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido227g&utm_term=Zacatecas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Zacatecas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Aguascalientes&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=BCN&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=BCS&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Campeche&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Chiapas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Chiahuahua&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Coahuila&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Colima&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Durango&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=EdoMex&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Guanajuato&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Guerrero&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Hidalgo&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Jalisco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Michoacan&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Nayarit&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=NvoLeon&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Oaxaca&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Puebla&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Queretaro&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=QuintanaRoo&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=SnLuisPotosi&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Sinaloa&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Sonora&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Tabasco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Tamaulipas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Tlaxcala&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Veracruz&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Yucatan&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/quesos/queso-parmesano-parma-85-g/00750151847635?utm_source=Website&utm_medium=Supercenter&utm_campaign=ParmesanoMolido85g&utm_term=Zacatecas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term= Zacatecas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Aguascalientes&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=BCN&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=BCS&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Campeche&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Chiapas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Chiahuahua&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Coahuila&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Colima&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Durango&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=EdoMex&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Guanajuato&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Guerrero&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Hidalgo&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Jalisco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Michoacan&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Nayarit&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=NvoLeon&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Oaxaca&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Puebla&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=QuintanaRoo&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=SnLuisPotosi&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Sinaloa&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Sonora&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Tabasco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Tamaulipas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Tlaxcala&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Veracruz&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Yucatan&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-100-g/00750151849135?utm_source=Website&utm_medium=Supercenter&utm_campaign=Serrano100g&utm_term=Zacatecas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term= Zacatecas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Aguascalientes&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=BCN&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=BCS&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Campeche&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Chiapas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Chiahuahua&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Coahuila&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Colima&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Durango&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=EdoMex&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Guerrero&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Hidalgo&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Michoacan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Nayarit&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=NvoLeon&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Oaxaca&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Puebla&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=QuintanaRoo&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=SnLuisPotosi&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Sinaloa&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Sonora&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Tabasco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Tamaulipas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Tlaxcala&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Veracruz&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Yucatan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/salami/salami-parma-tipo-genova-100-g/00750151849140?utm_source=Website&utm_medium=Supercenter&utm_campaign=Salami100g&utm_term=Zacatecas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term= Zacatecas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Aguascalientes&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=BCN&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=BCS&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Campeche&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Chiapas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Chiahuahua&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Coahuila&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Colima&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Durango&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=EdoMex&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Guerrero&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Hidalgo&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Michoacan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Nayarit&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=NvoLeon&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Oaxaca&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Puebla&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=QuintanaRoo&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=SnLuisPotosi&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Sinaloa&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Sonora&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Tabasco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Tamaulipas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Tlaxcala&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Veracruz&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Yucatan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/jamon-serrano-parma-charola-150-g/00750151849165?utm_source=Website&utm_medium=Supercenter&utm_campaign=CharolaSerrano150g&utm_term=Zacatecas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term= Zacatecas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Aguascalientes&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=BCN&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=BCS&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Campeche&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Chiapas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Chiahuahua&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Coahuila&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Colima&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Durango&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=EdoMex&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Guerrero&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Hidalgo&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Michoacan&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Nayarit&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=NvoLeon&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Oaxaca&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Puebla&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=QuintanaRoo&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=SnLuisPotosi&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Sinaloa&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Sonora&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Tabasco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Tamaulipas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Tlaxcala&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Veracruz&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Yucatan&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/chistorra-parma-350-g/00750151849171?utm_source=Website&utm_medium=Supercenter&utm_campaign=Chistorra350g&utm_term=Zacatecas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=BCN&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=BCS&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Chiapas&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=EdoMex&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Hidalgo&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Michoacan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Oaxaca&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Puebla&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=QuintanaRoo&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Sonora&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Tlaxcala&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-397-g/00750151849181?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddar397g&utm_term=Yucatan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=BCN&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=BCS&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Chiapas&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Chiahuahua&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMex&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Hidalgo&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Oaxaca&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Puebla&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=QuintanaRoo&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Sonora&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Tlaxcala&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/salchicha-parma-con-cheddar-y-jalapeno-397-g/00750151849182?utm_source=Website&utm_medium=Supercenter&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Yucatan&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term= Zacatecas&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Aguascalientes&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=BCN&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=BCS&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Campeche&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Chiapas&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Chiahuahua&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Coahuila&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Colima&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Durango&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=EdoMex&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Guanajuato&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Guerrero&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Hidalgo&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Michoacan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Nayarit&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=NvoLeon&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Oaxaca&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Puebla&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=QuintanaRoo&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=SnLuisPotosi&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Sinaloa&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Sonora&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Tabasco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Tamaulipas&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Tlaxcala&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Veracruz&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Yucatan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/lomo-y-pate/seleccion-gourmet-parma-lomo-embuchado-chorizo-pamplona-y-salami-genova-150-g/00750151849129?utm_source=Website&utm_medium=Supercenter&utm_campaign=SeleccionGourmet150g&utm_term=Zacatecas&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term= Zacatecas&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Aguascalientes&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=BCN&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=BCS&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Campeche&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Chiapas&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Chiahuahua&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Coahuila&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Colima&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Durango&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=EdoMex&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Guanajuato&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Guerrero&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Hidalgo&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Michoacan&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Nayarit&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=NvoLeon&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Oaxaca&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Puebla&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Queretaro&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=QuintanaRoo&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=SnLuisPotosi&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Sinaloa&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Sonora&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Tabasco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Tamaulipas&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Tlaxcala&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Veracruz&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Yucatan&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pepperoni-parma-100-g/00750151849128?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pepperoni100g&utm_term=Zacatecas&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term= Zacatecas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 1,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Aguascalientes&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 2,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=BCN&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 3,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=BCS&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 4,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Campeche&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 7,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Chiapas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 8,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Chiahuahua&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 5,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Coahuila&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 6,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Colima&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 10,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Durango&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=EdoMex&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 12,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Guerrero&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 13,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Hidalgo&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 16,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Michoacan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 18,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Nayarit&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 19,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=NvoLeon&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 20,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Oaxaca&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 21,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Puebla&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 23,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=QuintanaRoo&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 24,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=SnLuisPotosi&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 25,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Sinaloa&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 26,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Sonora&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 27,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Tabasco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 28,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Tamaulipas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 29,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Tlaxcala&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 30,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Veracruz&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 31,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Yucatan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 32,
            'retailer_id'	=> 6,
            'link'			=> 'https://super.walmart.com.mx/carnes-frias/pechuga-de-pavo-parma-180-g/00750151849176?utm_source=Website&utm_medium=Supercenter&utm_campaign=Pechuga180g&utm_term=Zacatecas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=BCS&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Aguascalientes&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Guanajuato&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Jalisco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Michoacan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Queretaro&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=SnLuisPotosi&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Durango&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 28,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Tamaulipas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Campeche&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Chiapas&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Quintana Roo&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Tabasco&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Veracruz&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Yucatan&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=CDMX&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=EdoMex&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Hidalgo&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Morelos&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Guerrero&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Oaxaca&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-parmesano-molido-Parma-227g/p/000000000003013984?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido227g&utm_term=Puebla&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=BCS&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Aguascalientes&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Guanajuato&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Jalisco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Michoacan&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 18,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Nayarit&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Queretaro&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 32,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Zacatecas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Durango&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=SnLuisPotosi&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 28,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Tamaulipas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Veracruz&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Campeche&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Chiapas&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Quintana Roo&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Tabasco&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Yucatan&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=CDMX&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=EdoMex&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Guerrero&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Hidalgo&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Morelos&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Oaxaca&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Puebla&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 29,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/L%C3%A1cteos-y-Huevos/Quesos/Maduros-y-Semimaduros/Queso-Parma-Parmesano-Molido-85g/p/000000000003013987?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=ParmesanoMolido85g&utm_term=Tlaxcala&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=BCS&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Aguascalientes&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Guanajuato&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Jalisco&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Michoacan&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Queretaro&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Quintana Roo&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Tabasco&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Yucatan&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=CDMX&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=EdoMex&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Hidalgo&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Morelos&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Guerrero&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Oaxaca&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Puebla&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-serrano-selecto-Parma-por-Kg/p/000000000003009306?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SerranoPorKg &utm_term=Veracruz&utm_content=9120'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=BCS&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Aguascalientes&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Jalisco&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Michoacan&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Queretaro&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Quintana Roo&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Tabasco&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Yucatan&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=CDMX&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=EdoMex&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Hidalgo&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Morelos&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Oaxaca&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Puebla&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 15,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Salami/Salami-tipo-G%C3%A9nova-Parma-por-Kg/p/000000000003009319&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalamiPorKg&utm_term=Veracruz&utm_content=9125'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=BCS&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Aguascalientes&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Jalisco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Michoacan&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=SnLuisPotosi&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Durango&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Campeche&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Chiapas&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Quintana Roo&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Tabasco&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Veracruz&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Yucatan&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=CDMX&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=EdoMex&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Hidalgo&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Morelos&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Guerrero&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Oaxaca&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Pepperoni-Parma-100g/p/000000000003369359?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pepperoni100g&utm_term=Puebla&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=BCS&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Aguascalientes&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Guanajuato&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Jalisco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Michoacan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Queretaro&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=SnLuisPotosi&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Durango&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Campeche&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Chiapas&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Quintana Roo&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Tabasco&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Veracruz&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Yucatan&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=CDMX&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=EdoMex&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Hidalgo&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Morelos&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Guerrero&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Oaxaca&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 16,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Selecci%C3%B3n-gourmet-Parma-150g/p/000000000003369313?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SeleccionGourmet150g&utm_term=Puebla&utm_content=9129'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=BCS&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Aguascalientes&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Guanajuato&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Jalisco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Michoacan&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Queretaro&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=SnLuisPotosi&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Durango&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Campeche&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Chiapas&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Quintana Roo&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Tabasco&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Veracruz&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Yucatan&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=CDMX&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=EdoMex&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Hidalgo&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Morelos&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Guerrero&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Oaxaca&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-Parma-100g/p/000000000003007434?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano100g&utm_term=Puebla&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=BCS&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Aguascalientes&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Guanajuato&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Jalisco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Michoacan&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Queretaro&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=SnLuisPotosi&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Durango&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Campeche&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Chiapas&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Quintana Roo&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Tabasco&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Veracruz&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Yucatan&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=CDMX&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=EdoMex&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Hidalgo&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Morelos&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Guerrero&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Oaxaca&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Jam%C3%B3n-serrano-premium-Parma-120g/p/000000000003223208?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Serrano120g&utm_term=Puebla&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=BCS&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Aguascalientes&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Guanajuato&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Jalisco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Michoacan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Queretaro&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=SnLuisPotosi&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Durango&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Campeche&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Chiapas&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Quintana Roo&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Tabasco&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Veracruz&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Yucatan&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=CDMX&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=EdoMex&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Hidalgo&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Morelos&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Guerrero&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Oaxaca&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Varios-Deli/Salami-tipo-G%C3%A9nova-Parma-100g/p/000000000003007431?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Salami100g&utm_term=Puebla&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=BCS&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Aguascalientes&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Guanajuato&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Jalisco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Michoacan&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Queretaro&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=SnLuisPotosi&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Durango&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 28,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Tamaulipas&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Campeche&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Chiapas&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Quintana Roo&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Tabasco&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Veracruz&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Yucatan&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=CDMX&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=EdoMex&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Hidalgo&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Morelos&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Guerrero&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Oaxaca&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/York/Jam%C3%B3n-de-pierna-York-Parma-200g/p/000000000003618281?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=York200g&utm_term=Puebla&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=BCS&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Aguascalientes&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Guanajuato&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Jalisco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Michoacan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Queretaro&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Durango&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Campeche&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Cancun&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Chiapas&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Quintana Roo&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Tabasco&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Veracruz&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Yucatan&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=CDMX&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=EdoMex&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Hidalgo&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Morelos&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Guerrero&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Oaxaca&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Jamones-Maduros/Charola-de-jam%C3%B3n-serrano-Parma-150g/p/000000000003007415?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=CharolaSerrano150g&utm_term=Puebla&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=BCS&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Aguascalientes&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Guanajuato&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Jalisco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Queretaro&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Durango&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Campeche&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Cancun&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Chiapas&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Quintana Roo&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Tabasco&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Veracruz&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Yucatan&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=CDMX&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=EdoMex&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Hidalgo&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Morelos&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Guerrero&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Oaxaca&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Chistorra-Parma-350g/p/000000000003007407?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Chistorra350g&utm_term=Puebla&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=BCS&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Aguascalientes&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Guanajuato&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Jalisco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Michoacan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Queretaro&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=SnLuisPotosi&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Durango&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 28,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Tamaulipas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Campeche&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Chiapas&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Quintana Roo&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Tabasco&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Veracruz&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Yucatan&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=CDMX&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=EdoMex&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Hidalgo&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Morelos&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Guerrero&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Oaxaca&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-de-Pavo-Parma-180g/p/000000000003618662?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=Pechuga180g&utm_term=Puebla&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=BCS&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Aguascalientes&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Guanajuato&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Jalisco&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Michoacan&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Queretaro&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=SnLuisPotosi&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Durango&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Campeche&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Chiapas&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Quintana Roo&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Tabasco&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Veracruz&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Yucatan&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=CDMX&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=EdoMex&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Hidalgo&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Morelos&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Guerrero&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Oaxaca&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Puebla&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 29,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Mortadelas-y-Pastas/Mortadela/Salchicha-ahumada-Parma-397g/p/000000000003188385?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaAhumada397g&utm_term=Tlaxcala&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=BCS&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Aguascalientes&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Guanajuato&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Jalisco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Michoacan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Queretaro&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=SnLuisPotosi&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Durango&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Campeche&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Chiapas&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Quintana Roo&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Tabasco&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Veracruz&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Yucatan&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=CDMX&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=EdoMex&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Hidalgo&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Morelos&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Guerrero&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Oaxaca&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-Parma-397g/p/000000000003188383?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddar397g&utm_term=Puebla&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 3,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=BCS&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 1,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Aguascalientes&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 11,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guanajuato&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 14,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Jalisco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 16,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Michoacan&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 22,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Queretaro&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 24,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=SnLuisPotosi&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 10,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Durango&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 4,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Campeche&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 7,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Chiapas&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 23,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Quintana Roo&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 27,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Tabasco&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 30,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Veracruz&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 31,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Yucatan&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=CDMX&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=EdoMex&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 13,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Hidalgo&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Morelos&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 12,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Guerrero&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 20,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Oaxaca&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 21,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/CH/Salchicha-queso-cheddar-con-jalape%C3%B1o-Parma-397g/p/000000000003188384?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Puebla&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-Parmagan-Reserva-kg/p/000000000003298275?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-Parmagan-Reserva-kg/p/000000000003298275?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n/Pechuga-de-pavo/Pechuga-Parmagan-Reserva-kg/p/000000000003298275?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 9,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-de-pierna-de-cerdo-selecci%C3%B3n-natural-Parma-por-Kg/p/000000000003298274?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=CDMX&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 15,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-de-pierna-de-cerdo-selecci%C3%B3n-natural-Parma-por-Kg/p/000000000003298274?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=EdoMex&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 5,
            'state_id'		=> 17,
            'retailer_id'	=> 12,
            'link'			=> 'https://www.chedraui.com.mx/Departamentos/S%C3%BAper/Salchichoner%C3%ADa-y-Encurtidos/Jam%C3%B3n-Serrano/Jam%C3%B3n-de-pierna-de-cerdo-selecci%C3%B3n-natural-Parma-por-Kg/p/000000000003298274?siteName=Sitio+de+Chedraui&utm_source=Website&utm_medium=Chedraui&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Morelos&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Chihuahua&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Coahuila&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Durango&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano100g&utm_term=Chihuahua&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano100g&utm_term=Coahuila&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 7,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano100g&utm_term=Durango&utm_content=9135'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano120g&utm_term=Chihuahua&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano120g&utm_term=Coahuila&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 9,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Serrano120g&utm_term=Durango&utm_content=9136'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Salami100g&utm_term=Chihuahua&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Salami100g&utm_term=Coahuila&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 14,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Salami100g&utm_term=Durango&utm_content=9140'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Chistorra350g&utm_term=Chihuahua&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Chistorra350g&utm_term=Coahuila&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 10,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Chistorra350g&utm_term=Durango&utm_content=9171'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoArgentino400g&utm_term=Chihuahua&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoArgentino400g&utm_term=Coahuila&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 13,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoArgentino400g&utm_term=Durango&utm_content=9161'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Chihuahua&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Coahuila&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 6,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=CharolaSerrano150g&utm_term=Durango&utm_content=9165'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Chihuahua&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 2,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=PiernaSeleccionNaturalPorKg&utm_term=Coahuila&utm_content=9192'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=York200g&utm_term=Chihuahua&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=York200g&utm_term=Coahuila&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 3,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=York200g&utm_term=Durango&utm_content=9158'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=LaminasParmesano140g&utm_term=Chihuahua&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=LaminasParmesano140g&utm_term=Coahuila&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 28,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=LaminasParmesano140g&utm_term=Durango&utm_content=9115'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=MiniSalchichas400g&utm_term=Chihuahua&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=MiniSalchichas400g&utm_term=Coahuila&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 24,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=MiniSalchichas400g&utm_term=Durango&utm_content=9178'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Chihuahua&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 1,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=PechugaSeleccionNaturalPorKg&utm_term=Coahuila&utm_content=9191'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pepperoni100g&utm_term=Chihuahua&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pepperoni100g&utm_term=Coahuila&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 17,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pepperoni100g&utm_term=Durango&utm_content=9128'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido227g&utm_term=Chihuahua&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido227g&utm_term=Coahuila&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 26,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido227g&utm_term=Durango&utm_content=7634'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido85g&utm_term=Chihuahua&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido85g&utm_term=Coahuila&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 25,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ParmesanoMolido85g&utm_term=Durango&utm_content=7635'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Chihuahua&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Coahuila&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 21,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddarJalapeño397g&utm_term=Durango&utm_content=9182'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaRes350g&utm_term=Chihuahua&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaRes350g&utm_term=Coahuila&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 22,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaRes350g&utm_term=Durango&utm_content=9188'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaAhumada397g&utm_term=Chihuahua&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaAhumada397g&utm_term=Coahuila&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 19,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaAhumada397g&utm_term=Durango&utm_content=9180'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddar397g&utm_term=Chihuahua&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddar397g&utm_term=Coahuila&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 20,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=SalchichaCheddar397g&utm_term=Durango&utm_content=9181'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=TresQuesos227g&utm_term=Chihuahua&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=TresQuesos227g&utm_term=Coahuila&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 27,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=TresQuesos227g&utm_term=Durango&utm_content=9109'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoSarta250g&utm_term=Chihuahua&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoSarta250g&utm_term=Coahuila&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 12,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=ChorizoSarta250g&utm_term=Durango&utm_content=9126'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 8,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pechuga180g&utm_term=Chihuahua&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 5,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pechuga180g&utm_term=Coahuila&utm_content=9176'
        ]);
        ProductRetailer::create([
            'product_id' 	=> 4,
            'state_id'		=> 10,
            'retailer_id'	=> 9,
            'link'			=> 'https://alsuper.com/buscar?q=Parma#&utm_source=Website&utm_medium=AlSuper&utm_campaign=Pechuga180g&utm_term=Durango&utm_content=9176'
        ]);

    }
}

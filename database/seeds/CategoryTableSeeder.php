<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'              => 'SERRANOS',
            'presentation'      => 'Te presentamos al',
            'name_secondary'    => 'JAMÓN SERRANO',
            'about_title'       => 'Sobre él',
            'about'             => 'Es heredero de la historia y tradición culinaria de España. Su nombre, Serrano, proviene de la palabra sierra. Y se debe a la costumbre de curar el jamón en parajes elevados de las sierras.
                                    <br>
                                    Proviene de cerdos blancos Duroc, Pietrain o Landrace criados en sierras exclusivas con los más altos estándares de excelencia para garantizar su calidad de vida. Estos son alimentados bajo un estricto régimen alimenticio de piensos con cereales que garantizan una dieta balanceada y saludable.',
            'legal'             => '10 meses de maduración. No contiene colorantes artificiales, GMS ni gluten.',
            'creation'          => 'Su preparación está a cargo de Maestros Jamoneros con un expertise inigualable que garantiza cada nota de sabor degustada. Es por ello que su creación es todo un proceso artesanal cuidado en cada paso.',
            
            'creation_item1_title'      => 'Salazón y lavado',
            'creation_item1_content'    => 'Es enterrado en sal premium durante un periodo de catorce días para deshidratarlo e intensificar su sabor. Pasado el periodo de salazón, se seca y lava minuciosamente para eliminar el exceso de sal y no alterar su sabor único.',
            'creation_item1_image'      => 'categories/serranos/serranos_creation1.jpg',
            'creation_item2_title'      => 'Post-salado y secado',
            'creation_item2_content'    => 'Es el momento más delicado de su creación. Para asegurar su calidad se debe mantener el jamón serrano a una temperatura y humedad adecuadas para evitar que su calidad se vea comprometida. Este proceso suele durar entre 45 y 90 días.',
            'creation_item2_image'      => 'categories/serranos/serranos_creation2.jpg',
            'creation_item3_title'      => 'Maduración',
            'creation_item3_content'    => 'Para terminar de perfeccionar su aroma, textura, sabor y matiz, es llevado a un lugar especial para que madure y pueda ser presentado en tu mesa tal como lo conoces.',
            'creation_item3_image'      => 'categories/serranos/serranos_creation3.jpg',

            'image_home'        => 'categories/serranos/serranos_home.png',
            'image_products'    => 'categories/serranos/serranos.png',

            'image_bg'    => 'categories/serranos/serranos_bg.png',
            'image_stamp'    => 'categories/serranos/serranos_stamp.png',

            'slug'              => 'serranos'
        ]);

        Category::create([
            'name'              => 'CHISTORRA Y CHORIZOS',
            'presentation'      => 'Te presentamos a las',
            'name_secondary'    => 'CHISTORRAS y CHORIZOS',
            'about_title'       => 'Sobre ellos',
            'about'             => 'La chistorra proviene del vasco "txistorra". Su origen es navarro y aragonés. Es creada con carne selecta de cerdo picada y fresca, adicionada con ajo, sal, pimentón y hierbas aromáticas.
                                    <br>
                                    Aunque el chorizo tiene un origen poco claro, es un clásico de la Península Ibérica hecho con carne de cerdo, pimentón y ajo, que se ha diversificado en Europa y América gracias a la cocina fusión.
                                    <br>
                                    Ambos son embutidos curados, versátiles y muy populares tanto en preparación y presentación.',
            'creation'          => 'La chistorra es un embutido español elaborado con carne picada fresca de cerdo, ajo, sal para intensificar su sabor; pimentón para darle su color rojo característico; y hierbas aromáticas como el perejil.',
            
            'creation_item1_title'      => 'Mezcla',
            'creation_item1_content'    => 'Se pican y mezclan la carne fresca seleccionada de cerdo junto con los condimentos; una vez lista la mezcla, se muele y embute en una tripa natural de cerdo. Después se amarra de ambos lados y las piezas se dejan escurrir.',
            'creation_item1_image'      => 'categories/chistorras_chorizos/chistorras_chorizos_creation1.jpg',
            'creation_item2_title'      => 'Secado',
            'creation_item2_content'    => 'Una vez listas, se colocan las piezas en el cuarto de secado a 20 °C y humedad de 90 %.',
            'creation_item2_image'      => 'categories/chistorras_chorizos/chistorras_chorizos_creation2.jpg',
            'creation_item3_title'      => 'Maduración',
            'creation_item3_content'    => 'Luego de 6 días de maduración, el producto tomará su delicioso sabor y textura, y estará listo para degustar.',
            'creation_item3_image'      => 'categories/chistorras_chorizos/chistorras_chorizos_creation3.jpg',
            
            'image_home'        => 'categories/chistorras_chorizos/chistorras_chorizos_home.png',
            'image_products'    => 'categories/chistorras_chorizos/chistorras_chorizos.png',

            'image_bg'    => 'categories/chistorras_chorizos/chistorras_chorizos_bg.png',
            'image_stamp'    => 'categories/chistorras_chorizos/chistorras_chorizos_stamp.png',
            
            'slug'              => 'chistorras-y-chorizos'
        ]);

        Category::create([
            'name'              => 'SALAMI Y MADURADOS',
            'presentation'      => 'Te presentamos al',
            'name_secondary'    => 'SALAMI Y LOS MADURADOS',
            'about_title'       => 'Sobre ellos',
            'about'             => 'El salami proviene del italiano "salame" y significa "embutido salado". Se elabora con una fina mezcla de carne vacuna y porcina sazonadas con sal, posteriormente se ahuman y curan al aire.
                                    <br>
                                    Por su parte, los madurados se refiere a diversos productos embutidos que son sometidos a añejamiento para mejorar significativamente su textura, olor y sabor.
                                    <br>
                                    Como dato curioso, se sabe que este proceso culinario se remonta al siglo XIX, mismo que se ha mejorado con el paso del tiempo.',
            'creation'          => 'El salami es un embutido italiano. Se elabora con un proceso artesanal donde se unen carne selecta de cerdo, condimentos, el ahumado y un curado al aire por un periodo de maduración de 40 días.',
            
            'creation_item1_title'      => 'Selección y Molido',
            'creation_item1_content'    => 'Se elige la mejor carne de cerdo, se corta en trozos y condimenta con sal, pimienta blanca y negra. Posteriormente se muele y se embute en una tripa natural para el proceso de curación.',
            'creation_item1_image'      => 'categories/salamis/salamis_creation1.jpg',
            'creation_item2_title'      => 'Embutido y Maduración',
            'creation_item2_content'    => 'Una vez embutido, el salami pasa a los cuartos de maduración, donde su sabor comenzará a perfeccionarse, con temperaturas entre 29 a 39 ºC durante 40 días.',
            'creation_item2_image'      => 'categories/salamis/salamis_creation2.jpg',
            'creation_item3_title'      => 'Secado',
            'creation_item3_content'    => 'Después de 40 días, la humedad, la temperatura y el aire harán su trabajo para que el salami logre su característico color rojizo, así como un sabor perfecto.',
            'creation_item3_image'      => 'categories/salamis/salamis_creation3.jpg',

            'image_home'        => 'categories/salamis/salamis_home.png',
            'image_products'    => 'categories/salamis/salamis.png',

            'image_bg'    => 'categories/salamis/salamis_bg.png',
            'image_stamp'    => 'categories/salamis/salamis_stamp.png',

            'slug'              => 'salamis-y-madurados'
        ]);
        Category::create([
            'name'              => 'PARMESANOS',
            'presentation'      => 'Te presentamos a los',
            'name_secondary'    => 'PARMESANOS',
            'about_title'       => 'Sobre ellos',
            'about'             => 'Originario de Italia y conocido como "Parmigiano Reggiano". Según la creencia popular, nació en la Edad Media en Bibbiano, una provincia de Reggio Emilia, bajo la diócesis de Parma donde adquirió su nombre.
                                    <br>
                                    Su fabricación es con leche producida por vacas frisonas. Su maduración ideal es de 12 meses, aunque puede llegar a los 36, 80 o 120.
                                    <br>
                                    Es catalogado con un sabor único umami; y normalmente su presentación en platillos es rallada o fundida, aunque puede servirse en láminas.',
            'creation'          => 'Desde la Edad Media hasta nuestra época, los ingredientes para elaborar queso parmesano se han mantenido inalterados, así como el proceso de maduración que le otorga su textura y sabor únicos.',
            
            'creation_item1_title'      => 'Selección de leche',
            'creation_item1_content'    => 'Para elaborar este increíble queso es necesario comenzar por recolectar leche selecta de vaca.',
            'creation_item1_image'      => 'categories/parmesanos/parmesanos_creation1.jpg',
            'creation_item2_title'      => 'Mezclado y Drenado',
            'creation_item2_content'    => 'Una vez recolectada debe calentarse a 50 ºC, cuando ha hervido, se deja reposar por una hora. El producto es colgado para drenarse y después se transfiere a moldes de madera para que tome su forma.',
            'creation_item2_image'      => 'categories/parmesanos/parmesanos_creation2.jpg',
            'creation_item3_title'      => 'Salmuera y fuego',
            'creation_item3_content'    => 'Los quesos son sumergidos en una solución de sal marina por 24 horas, después se llevan a las salas de curado donde permanecen de 10 a 24 meses. Por último se limpian y cepillan, quedando listos para su consumo.',
            'creation_item3_image'      => 'categories/parmesanos/parmesanos_creation3.jpg',
            
            'image_home'        => 'categories/parmesanos/parmesanos_home.png',
            'image_products'    => 'categories/parmesanos/parmesanos.png',

            'image_bg'    => 'categories/parmesanos/parmesanos_bg.png',
            'image_stamp'    => 'categories/parmesanos/parmesanos_stamp.png',

            'slug'              => 'parmesanos'
        ]);
        Category::create([
            'name'              => 'JAMONES Y PECHUGAS',
            'presentation'      => 'Te presentamos a los',
            'name_secondary'    => 'JAMONES Y PECHUGAS',
            'about_title'       => 'Sobre ellos',
            'about'             => 'Elaborada con ingredientes 100% naturales, horneada de manera artesanal. Cuenta con una Pechuga de Pavo y Pierna de Cerdo.
                                    <br>
                                    Seleccionamos cada Pierna de Cerdo y Pechuga de Pavo. Inyectamos individualmente cada pieza con salmuera, para darle aún mayor jugosidad.',
            'creation'          => 'Creamos productos con ingredientes 100 % naturales que se mezclan y hornean de manera artesanal para garantizar sabor, calidad y nutrición en cada pechuga de pavo y pierna de cerdo.',
            
            'creation_item1_title'      => 'Selección y Salmuera',
            'creation_item1_content'    => 'Se elige estrictamente cada pieza de pierna de cerdo y pechuga de pavo. Después se inyectan individualmente con salmuera, lo cual ayuda a darle mayor jugosidad a cada producto.',
            'creation_item1_image'      => 'categories/jamones/jamones_creation1.jpg',
            'creation_item2_title'      => 'Horneado',
            'creation_item2_content'    => 'Las piezas son colocadas dentro de una malla de hilos donde se hornean a cocción lenta durante 8 horas. Al finalizar esta etapa se obtienen productos que conservan su jugosidad y su delicioso sabor natural.',
            'creation_item2_image'      => 'categories/jamones/jamones_creation2.jpg',
            'creation_item3_title'      => 'Empacado',
            'creation_item3_content'    => 'Una vez listos, se les retira la malla de hilos con la que fueron horneados. Por último se empaca y sellan al vacío para que conservar el delicioso sabor que han adquirido, así como resguardar su calidad para tu consumo.',
            'creation_item3_image'      => 'categories/jamones/jamones_creation3.jpg',
            
            'image_home'        => 'categories/jamones/jamones_home.png',
            'image_products'    => 'categories/jamones/jamones.png',

            'image_bg'    => 'categories/jamones/jamones_bg.png',
            'image_stamp'    => 'categories/jamones/jamones_stamp.png',

            'slug'              => 'jamones-y-pechugas'
        ]);
        Category::create([
            'name'              => 'SALCHICHAS',
            'presentation'      => 'Te presentamos a las',
            'name_secondary'    => 'SALCHICHAS',
            'about_title'       => 'Sobre ellas',
            'about'             => 'La palabra salchicha proviene del italiano "salsiccia", que significa salado. Es un embutido salado preparado a base de carne y especias para realzar su sabor. Se sabe de ellas desde la época de los babilonios.
                                    <br>
                                    Y hoy día es uno de los platillos más populares en la cocina, ya sea en preparaciones complejas o sencillas. Su gran variedad depende de la carne con la que se elabora, su curación y maduración, dando origen a más de 20 variedades, incluso con denominación de origen.',
            'creation'          => 'Elaboradas con los mejores cortes de carne de cerdo, res, pavo, especias e ingredientes de calidad, las salchichas son muy versátiles y prácticas para disfrutar como snack o como plato fuerte.',
            
            'creation_item1_title'      => 'Mezcla y Molido',
            'creation_item1_content'    => 'Se pica la carne seleccionada fresca, la cual se mezcla con sal, especias e ingredientes especiales para dar su sabor característico a cada salchicha. Después se muele la mezcla hasta conseguir la consistencia deseada.',
            'creation_item1_image'      => 'categories/salchichas/salchichas_creation1.jpg',
            'creation_item2_title'      => 'Embutido',
            'creation_item2_content'    => 'Cuando la mezcla tiene la consistencia ideal, es embutida en una tripa selecta, la cual, posteriormente se cocina en un horno o con agua hirviendo por algunas horas.',
            'creation_item2_image'      => 'categories/salchichas/salchichas_creation2.jpg',
            'creation_item3_title'      => 'Enfriamiento y Empaquetado',
            'creation_item3_content'    => 'Según el tipo de salchicha que se elabore, estas serán retiradas del horno o agua donde se cocinaron para que se enfríen y, de esta manera, estén listas para empacarse y consumirse.',
            'creation_item3_image'      => 'categories/salchichas/salchichas_creation3.jpg',
            
            'image_home'        => 'categories/salchichas/salchichas_home.png',
            'image_products'    => 'categories/salchichas/salchichas.png',

            'image_bg'    => 'categories/salchichas/salchichas_bg.png',
            'image_stamp'    => 'categories/salchichas/salchichas_stamp.png',

            'slug'              => 'salchichas'
        ]);
    }
}

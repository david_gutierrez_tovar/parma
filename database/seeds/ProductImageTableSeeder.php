<?php

use App\ProductImage;
use Illuminate\Database\Seeder;

class ProductImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Jamones y Pechugas
        ProductImage::create([
            'product_id'        => 1,
            'image'             => 'products/pechuga_pavo/pechuga_pavo_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 1,
            'image'             => 'products/pechuga_pavo/pechuga_pavo_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 1,
            'image'             => 'products/pechuga_pavo/pechuga_pavo_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 2,
            'image'             => 'products/pierna_cerdo/pierna_cerdo_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 2,
            'image'             => 'products/pierna_cerdo/pierna_cerdo_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 2,
            'image'             => 'products/pierna_cerdo/pierna_cerdo_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 3,
            'image'             => 'products/jamon_york_200/jamon_york_200_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 3,
            'image'             => 'products/jamon_york_200/jamon_york_200_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 3,
            'image'             => 'products/jamon_york_200/jamon_york_200_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 4,
            'image'             => 'products/pechuga_pavo_triple_180/pechuga_pavo_triple_180_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 4,
            'image'             => 'products/pechuga_pavo_triple_180/pechuga_pavo_triple_180_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 4,
            'image'             => 'products/pechuga_pavo_triple_180/pechuga_pavo_triple_180_p3.jpg',
        ]);



        // Serranos
        ProductImage::create([
            'product_id'        => 5,
            'image'             => 'products/jamon_serrano_pierna/jamon_serrano_pierna_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 5,
            'image'             => 'products/jamon_serrano_pierna/jamon_serrano_pierna_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 5,
            'image'             => 'products/jamon_serrano_pierna/jamon_serrano_pierna_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 6,
            'image'             => 'products/jamon_serrano_charola/jamon_serrano_charola_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 6,
            'image'             => 'products/jamon_serrano_charola/jamon_serrano_charola_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 6,
            'image'             => 'products/jamon_serrano_charola/jamon_serrano_charola_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 7,
            'image'             => 'products/jamon_serrano_100/jamon_serrano_100_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 7,
            'image'             => 'products/jamon_serrano_100/jamon_serrano_100_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 7,
            'image'             => 'products/jamon_serrano_100/jamon_serrano_100_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 8,
            'image'             => 'products/jamon_serrano_500/jamon_serrano_500_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 8,
            'image'             => 'products/jamon_serrano_500/jamon_serrano_500_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 8,
            'image'             => 'products/jamon_serrano_500/jamon_serrano_500_p3.jpg',
        ]);



        ProductImage::create([
            'product_id'        => 9,
            'image'             => 'products/jamon_serrano_120/jamon_serrano_120_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 9,
            'image'             => 'products/jamon_serrano_120/jamon_serrano_120_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 9,
            'image'             => 'products/jamon_serrano_120/jamon_serrano_120_p3.jpg',
        ]);




        // Chistorras y Chorizos
        ProductImage::create([
            'product_id'        => 10,
            'image'             => 'products/chistorra_350/chistorra_350_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 10,
            'image'             => 'products/chistorra_350/chistorra_350_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 10,
            'image'             => 'products/chistorra_350/chistorra_350_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 11,
            'image'             => 'products/chistorra_800/chistorra_800_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 11,
            'image'             => 'products/chistorra_800/chistorra_800_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 11,
            'image'             => 'products/chistorra_800/chistorra_800_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 12,
            'image'             => 'products/chorizo_sarta_250/chorizo_sarta_250_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 12,
            'image'             => 'products/chorizo_sarta_250/chorizo_sarta_250_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 12,
            'image'             => 'products/chorizo_sarta_250/chorizo_sarta_250_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 13,
            'image'             => 'products/chorizo_argentino_400/chorizo_argentino_400_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 13,
            'image'             => 'products/chorizo_argentino_400/chorizo_argentino_400_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 13,
            'image'             => 'products/chorizo_argentino_400/chorizo_argentino_400_p3.jpg',
        ]);



        // Salamis y Madurados
        ProductImage::create([
            'product_id'        => 14,
            'image'             => 'products/salami_genova_100/salami_genova_100_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 14,
            'image'             => 'products/salami_genova_100/salami_genova_100_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 14,
            'image'             => 'products/salami_genova_100/salami_genova_100_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 15,
            'image'             => 'products/salami_genova_1000/salami_genova_1000_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 15,
            'image'             => 'products/salami_genova_1000/salami_genova_1000_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 15,
            'image'             => 'products/salami_genova_1000/salami_genova_1000_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 16,
            'image'             => 'products/seleccion_gourmet_150/seleccion_gourmet_150_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 16,
            'image'             => 'products/seleccion_gourmet_150/seleccion_gourmet_150_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 16,
            'image'             => 'products/seleccion_gourmet_150/seleccion_gourmet_150_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 17,
            'image'             => 'products/pepperoni_100/pepperoni_100_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 17,
            'image'             => 'products/pepperoni_100/pepperoni_100_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 17,
            'image'             => 'products/pepperoni_100/pepperoni_100_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 18,
            'image'             => 'products/salami_genova_400/salami_genova_400_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 18,
            'image'             => 'products/salami_genova_400/salami_genova_400_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 18,
            'image'             => 'products/salami_genova_400/salami_genova_400_p3.jpg',
        ]);


        // Salchichas
        ProductImage::create([
            'product_id'        => 19,
            'image'             => 'products/salchicha_ahumada_397/salchicha_ahumada_397_p1.png',
        ]);
        ProductImage::create([
            'product_id'        => 19,
            'image'             => 'products/salchicha_ahumada_397/salchicha_ahumada_397_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 19,
            'image'             => 'products/salchicha_ahumada_397/salchicha_ahumada_397_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 20,
            'image'             => 'products/salchicha_cheddar_397/salchicha_cheddar_397_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 20,
            'image'             => 'products/salchicha_cheddar_397/salchicha_cheddar_397_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 20,
            'image'             => 'products/salchicha_cheddar_397/salchicha_cheddar_397_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 21,
            'image'             => 'products/salchicha_cheddar_jalapeno_397/salchicha_cheddar_jalapeno_397_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 21,
            'image'             => 'products/salchicha_cheddar_jalapeno_397/salchicha_cheddar_jalapeno_397_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 21,
            'image'             => 'products/salchicha_cheddar_jalapeno_397/salchicha_cheddar_jalapeno_397_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 22,
            'image'             => 'products/salchicha_res_350/salchicha_res_350_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 22,
            'image'             => 'products/salchicha_res_350/salchicha_res_350_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 22,
            'image'             => 'products/salchicha_res_350/salchicha_res_350_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 23,
            'image'             => 'products/salchicha_pechuga_pavo_350/salchicha_pechuga_pavo_350_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 23,
            'image'             => 'products/salchicha_pechuga_pavo_350/salchicha_pechuga_pavo_350_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 23,
            'image'             => 'products/salchicha_pechuga_pavo_350/salchicha_pechuga_pavo_350_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 24,
            'image'             => 'products/mini_salchichas_ahumadas_400/mini_salchichas_ahumadas_400_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 24,
            'image'             => 'products/mini_salchichas_ahumadas_400/mini_salchichas_ahumadas_400_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 24,
            'image'             => 'products/mini_salchichas_ahumadas_400/mini_salchichas_ahumadas_400_p3.jpg',
        ]);



        // Parmesanos
        ProductImage::create([
            'product_id'        => 25,
            'image'             => 'products/parmesano_85/parmesano_85_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 25,
            'image'             => 'products/parmesano_85/parmesano_85_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 25,
            'image'             => 'products/parmesano_85/parmesano_85_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 26,
            'image'             => 'products/parmesano_227/parmesano_227_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 26,
            'image'             => 'products/parmesano_227/parmesano_227_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 26,
            'image'             => 'products/parmesano_227/parmesano_227_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 27,
            'image'             => 'products/tres_quesos_227/tres_quesos_227_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 27,
            'image'             => 'products/tres_quesos_227/tres_quesos_227_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 27,
            'image'             => 'products/tres_quesos_227/tres_quesos_227_p3.jpg',
        ]);


        ProductImage::create([
            'product_id'        => 28,
            'image'             => 'products/laminas_parmesanos_140/laminas_parmesanos_140_p1.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 28,
            'image'             => 'products/laminas_parmesanos_140/laminas_parmesanos_140_p2.jpg',
        ]);
        ProductImage::create([
            'product_id'        => 28,
            'image'             => 'products/laminas_parmesanos_140/laminas_parmesanos_140_p3.jpg',
        ]);


    }
}

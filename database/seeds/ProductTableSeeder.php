<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Jamones y Pechugas
        Product::create([
            'name'              => 'Pechuga de Pavo Parma',
            'slug'              => 'pechuga-de-pavo-parma',
            'presentation'      => 'Por Kilo',
            'description'       => 'Nuestra deliciosa Pechuga de Pavo Parma es elaborada en un proceso artesanal con un ahumado natural, es un producto sin conservadores, sin colorantes ni gluten.',
            'image'             => 'products/pechuga_pavo/pechuga_pavo.png',
            'image_thumb'             => 'products/pechuga_pavo/pechuga_pavo_thumb.png',
            'category_id'       => 5
        ]);
        Product::create([
            'name'              => 'Pierna de Cerdo Parma',
            'slug'              => 'pierna-de-cerdo-parma',
            'presentation'      => 'Por Kilo',
            'description'       => 'Nuestra deliciosa Pierna de Cerdo Parma es elaborada en un proceso artesanal con un horneado natural, es un producto sin conservadores, sin colorantes ni gluten.',
            'image'             => 'products/pierna_cerdo/pierna_cerdo.png',
            'image_thumb'             => 'products/pierna_cerdo/pierna_cerdo_thumb.png',
            'category_id'       => 5
        ]);
        Product::create([
            'name'              => 'Jamón de Pierna York Parma',
            'slug'              => 'jamon-de-pierna-york-parma-200g',
            'presentation'      => '200g',
            'description'       => 'El Jamón de Pierna York Parma es alto proteína y bajo en grasa, perfecto para complementar tus creaciones en la cocina.',
            'image'             => 'products/jamon_york_200/jamon_york_200.png',
            'image_thumb'             => 'products/jamon_york_200/jamon_york_200_thumb.png',
            'category_id'       => 5
        ]);
        Product::create([
            'name'              => 'Pechuga de Pavo Parma',
            'slug'              => 'Pechuga-de-Pavo-180g',
            'presentation'      => '180g',
            'description'       => 'Nuestra deliciosa Pechuga de Pavo Parma es el aliado perfecto para darle sabor a tus recetas',
            'image'             => 'products/pechuga_pavo_triple_180/pechuga_pavo_triple_180.png',
            'image_thumb'             => 'products/pechuga_pavo_triple_180/pechuga_pavo_triple_180_thumb.png',
            'category_id'       => 5
        ]);

        // Serranos
        Product::create([
            'name'              => 'Jamón Serrano Pierna Sin Hueso',
            'slug'              => 'jamon-serrano-pierna-sin-hueso-pieza',
            'presentation'      => 'Por Kilo',
            'description'       => 'Más de 10 meses de maduración, el serrano más reconocido en el mercado. Deleitate con el delicioso sabor del serrano Parma y dale un giro a tus platillos',
            'image'             => 'products/jamon_serrano_pierna/jamon_serrano_pierna.png',
            'image_thumb'             => 'products/jamon_serrano_pierna/jamon_serrano_pierna_thumb.png',
            'category_id'       => 1
        ]);
        Product::create([
            'name'              => 'Charola de Jamón Serrano Parma',
            'slug'              => 'charola-de-jamon-serrano-parma-150g',
            'presentation'      => '150g',
            'description'       => 'Más de 10 meses de maduración, con prácticas rebanadas que te acompañarán en los mejores momentos. Lleva tus platillos al siguiente nivel con el serrano Parma',
            'image'             => 'products/jamon_serrano_charola/jamon_serrano_charola.png',
            'image_thumb'             => 'products/jamon_serrano_charola/jamon_serrano_charola_thumb.png',
            'category_id'       => 1
        ]);
        Product::create([
            'name'              => 'Jamón Serrano Parma',
            'slug'              => 'jamon-serrano-parma-100g',
            'presentation'      => '100g',
            'description'       => 'Con más de 10 meses de maduración, prácticas rebanadas con separador intermedio y un delicioso sabor. Ideal para consentirte y deleitarte.',
            'image'             => 'products/jamon_serrano_100/jamon_serrano_100.png',
            'image_thumb'             => 'products/jamon_serrano_100/jamon_serrano_100_thumb.png',
            'category_id'       => 1
        ]);
        Product::create([
            'name'              => 'Jamón Serrano Parma',
            'slug'              => 'jamon-serrano-parma-500g',
            'presentation'      => '500g',
            'description'       => 'Con más de 10 meses de maduración y una presentación pensada para compartir con familia y amigos. Ideal para club de precios.',
            'image'             => 'products/jamon_serrano_500/jamon_serrano_500.png',
            'image_thumb'             => 'products/jamon_serrano_500/jamon_serrano_500_thumb.png',
            'category_id'       => 1
        ]);
        Product::create([
            'name'              => 'Jamón Serrano Parma',
            'slug'              => 'jamon-serrano-parma-120g',
            'presentation'      => '120g',
            'description'       => 'Más de 10 meses de maduración, con prácticas rebanadas que te acompañaran en los mejores momentos.Lleva tus platillos al siguiente nivel con el serrano Parma',
            'image'             => 'products/jamon_serrano_120/jamon_serrano_120.png',
            'image_thumb'             => 'products/jamon_serrano_120/jamon_serrano_120_thumb.png',
            'category_id'       => 1
        ]);

        // Chistorras y Chorizos
        Product::create([
            'name'              => 'Chistorra Parma',
            'slug'              => 'chistorra-parma-350g',
            'presentation'      => '350g',
            'description'       => 'La deliciosa chistorra Parma en prácticas tiras listas para usar. Una increíble combinación de especias, chiles y carne de cerdo.',
            'image'             => 'products/chistorra_350/chistorra_350.png',
            'image_thumb'             => 'products/chistorra_350/chistorra_350_thumb.png',
            'category_id'       => 2
        ]);
        Product::create([
            'name'              => 'Chistorra Parma',
            'slug'              => 'chistorra-parma-800g',
            'presentation'      => '800g',
            'description'       => 'La deliciosa combinación de sabores de especias, chiles y carne de cerdo en prácticas tiras y una presentación ideal para compartir.',
            'image'             => 'products/chistorra_800/chistorra_800.png',
            'image_thumb'             => 'products/chistorra_800/chistorra_800_thumb.png',
            'category_id'       => 2
        ]);
        Product::create([
            'name'              => 'Chorizo Sarta Parma',
            'slug'              => 'chorizo-sarta-parma-250g',
            'presentation'      => '250g',
            'description'       => 'Lleva a tu sartén un producto elaborado artesanalmente y con más de 20 días de maduración. Disfruta del delicioso sabor del Chorizo Sarta de Parma.',
            'image'             => 'products/chorizo_sarta_250/chorizo_sarta_250.png',
            'image_thumb'             => 'products/chorizo_sarta_250/chorizo_sarta_250_thumb.png',
            'category_id'       => 2
        ]);
        Product::create([
            'name'              => 'Chorizo Argentino Parma',
            'slug'              => 'chorizo-argentino-parma-400g',
            'presentation'      => '400g',
            'description'       => 'Disfruta del delicioso Chorizo Argentino Parma y acompaña tus plaltillos con un toque de sabor diferente.',
            'image'             => 'products/chorizo_argentino_400/chorizo_argentino_400.png',
            'image_thumb'             => 'products/chorizo_argentino_400/chorizo_argentino_400_thumb.png',
            'category_id'       => 2
        ]);

        // Salamis y Madurados
        Product::create([
            'name'              => 'Salami Génova Parma',
            'slug'              => 'salami-genova-100g',
            'presentation'      => '100g',
            'description'       => 'Acompaña tus platillos con el delicioso Salami Genova con más de 45 días de maduración y elaborado con trocitos de pimienta negra.',
            'image'             => 'products/salami_genova_100/salami_genova_100.png',
            'image_thumb'             => 'products/salami_genova_100/salami_genova_100_thumb.png',
            'category_id'       => 3
        ]);
        Product::create([
            'name'              => 'Salami Génova Parma',
            'slug'              => 'salami-genova-por-kg',
            'presentation'      => 'Por Kilo',
            'description'       => 'Un delicioso Salami Genova con más de 45 días de maduración y elaborado con trocitos de pimienta negra para llenar de sabor tu paladar.',
            'image'             => 'products/salami_genova_1000/salami_genova_1000.png',
            'image_thumb'             => 'products/salami_genova_1000/salami_genova_1000_thumb.png',
            'category_id'       => 3
        ]);
        Product::create([
            'name'              => 'Selección Gourmet Parma',
            'slug'              => 'seleccion-gourmet-parma-150g',
            'presentation'      => '150g',
            'description'       => 'Conoce la versatilidad de la selección Gourmet de Parma en tu cocina que incluye: lomo embuchado, chorizo pamplona y salami génova.',
            'image'             => 'products/seleccion_gourmet_150/seleccion_gourmet_150.png',
            'image_thumb'             => 'products/seleccion_gourmet_150/seleccion_gourmet_150_thumb.png',
            'category_id'       => 3
        ]);
        Product::create([
            'name'              => 'Pepperoni Parma',
            'slug'              => 'pepperoni-parma-100g',
            'presentation'      => '100g',
            'description'       => 'Que tus platillos brillen con nuestro delicioso pepperoni de 45 días de maduración, no tiene colorantes artificiales ni conservadores.',
            'image'             => 'products/pepperoni_100/pepperoni_100.png',
            'image_thumb'             => 'products/pepperoni_100/pepperoni_100_thumb.png',
            'category_id'       => 3
        ]);
        Product::create([
            'name'              => 'Salami Génova Parma',
            'slug'              => 'salami-genova-400g',
            'presentation'      => '400g',
            'description'       => 'Una presentación familiar de nuestro Salami Genova con más de 45 días de maduración, ideal para todas tus recetas.',
            'image'             => 'products/salami_genova_400/salami_genova_400.png',
            'image_thumb'             => 'products/salami_genova_400/salami_genova_400_thumb.png',
            'category_id'       => 3
        ]);

        // Salchichas
        Product::create([
            'name'              => 'Salchicha Ahumada Parma',
            'slug'              => 'salchicha-ahumada-parma-397g',
            'presentation'      => '397g',
            'description'       => 'Ideal para hot dogs y para asar, nuestra Salchicha Ahumada Parma tiene un sabor ahumado natural con una textura y mordida firme.',
            'image'             => 'products/salchicha_ahumada_397/salchicha_ahumada_397.png',
            'image_thumb'             => 'products/salchicha_ahumada_397/salchicha_ahumada_397_thumb.png',
            'category_id'       => 6
        ]);
        Product::create([
            'name'              => 'Salchicha con Cheddar Parma',
            'slug'              => 'salchicha-con-cheddar-parma-397g',
            'presentation'      => '397g',
            'description'       => 'Sabor ahumado natural con deliciosos trocitos de queso cheddar, ideal para las carnes asadas con una textura y mordida firme.',
            'image'             => 'products/salchicha_cheddar_397/salchicha_cheddar_397.png',
            'image_thumb'             => 'products/salchicha_cheddar_397/salchicha_cheddar_397_thumb.png',
            'category_id'       => 6
        ]);
        Product::create([
            'name'              => 'Salchicha con Cheddar y Jalapeño Parma',
            'slug'              => 'salchicha-con-cheddar-y-jalapeño-parma-397g',
            'presentation'      => '397g',
            'description'       => 'Sabor ahumado natural con deliciosos trocitos de queso cheddar y jalapeño, ideal para las carnes asadas con una textura y mordida firme.',
            'image'             => 'products/salchicha_cheddar_jalapeno_397/salchicha_cheddar_jalapeno_397.png',
            'image_thumb'             => 'products/salchicha_cheddar_jalapeno_397/salchicha_cheddar_jalapeno_397_thumb.png',
            'category_id'       => 6
        ]);
        Product::create([
            'name'              => 'Salchicha de Res Parma',
            'slug'              => 'salchicha-de-res-parma-350g',
            'presentation'      => '350g',
            'description'       => 'Degusta de tus platillos con nuestra Salchicha de Res Parma hecha con cortes de res seleccionada y ahumada naturalmente.',
            'image'             => 'products/salchicha_res_350/salchicha_res_350.png',
            'image_thumb'             => 'products/salchicha_res_350/salchicha_res_350_thumb.png',
            'category_id'       => 6
        ]);
        Product::create([
            'name'              => 'Salchicha de Pechuga de Pavo Parma',
            'slug'              => 'salchicha-de-pechuga-de-pavo-parma-350g',
            'presentation'      => '350g',
            'description'       => 'Disfruta de nuestras salchichas elaboradas artesanalmente con 100% carne de pavo seleccionada, sin gluten y sin GMS.',
            'image'             => 'products/salchicha_pechuga_pavo_350/salchicha_pechuga_pavo_350.png',
            'image_thumb'             => 'products/salchicha_pechuga_pavo_350/salchicha_pechuga_pavo_350_thumb.png',
            'category_id'       => 6
        ]);
        Product::create([
            'name'              => 'Salchichas Mini Ahumadas Parma',
            'slug'              => 'salchichas-mini-ahumadas-parma-400g',
            'presentation'      => '400g',
            'description'       => 'Disfruta de tus mejores eventos con nuestras salchichas cocteleras de carne de cerdo seleccionada con un ahumado delicioso.',
            'image'             => 'products/mini_salchichas_ahumadas_400/mini_salchichas_ahumadas_400.png',
            'image_thumb'             => 'products/mini_salchichas_ahumadas_400/mini_salchichas_ahumadas_400_thumb.png',
            'category_id'       => 6
        ]);

        // Parmesanos
        Product::create([
            'name'              => 'Queso Parmesano Parma',
            'slug'              => 'queso-parmesano-parma-85g',
            'presentation'      => '85g',
            'description'       => 'Nuestro Queso Parmesano Parma con 10 meses de añejamiento y elaborado 100% de leche en una presentación más práctica de 85 g.',
            'image'             => 'products/parmesano_85/parmesano_85.png',
            'image_thumb'             => 'products/parmesano_85/parmesano_85_thumb.png',
            'category_id'       => 4
        ]);
        Product::create([
            'name'              => 'Queso Parmesano Parma',
            'slug'              => 'queso-parmesano-parma-227g',
            'presentation'      => '227g',
            'description'       => 'El ingrediente especial para acompañar un sin número de platillos, nuestro Queso Parmesano Parma con 10 meses de añejamiento y elaborado 100% de leche.',
            'image'             => 'products/parmesano_227/parmesano_227.png',
            'image_thumb'             => 'products/parmesano_227/parmesano_227_thumb.png',
            'category_id'       => 4
        ]);
        Product::create([
            'name'              => 'Selección Tres Quesos Parma',
            'slug'              => 'seleccion-tres-quesos-parma-227g',
            'presentation'      => '227g',
            'description'       => 'Conoce nuestra Selección de Quesos: Parmesano, y Romano y haz magia en la cocina.',
            'image'             => 'products/tres_quesos_227/tres_quesos_227.png',
            'image_thumb'             => 'products/tres_quesos_227/tres_quesos_227_thumb.png',
            'category_id'       => 4
        ]);
        Product::create([
            'name'              => 'Láminas Parmesano Parma',
            'slug'              => 'laminas-parmesano-parma-140g',
            'presentation'      => '140g',
            'description'       => 'Nuestras Láminas de Queso Parmesano Parma están listas para usarse en una presentación práctica y exquisita. ',
            'image'             => 'products/laminas_parmesanos_140/laminas_parmesanos_140.png',
            'image_thumb'             => 'products/laminas_parmesanos_140/laminas_parmesanos_140_thumb.png',
            'category_id'       => 4
        ]);

        // Agregados Despues
        // Product::create([
        //     'name'              => 'Chistorra de Pavo Parma',
        //     'slug'              => 'chistorra-de-pavo-parma-800g',
        //     'presentation'      => '800g',
        //     'description'       => 'Prácticas tiras de chistorra, con el delicioso sabor de la carne de cerdo, pimentón, sal y ajo. No contiene gluten.',
        //     'image'             => 'products/chistorra_pavo_800/chistorra_pavo_800.png',
        //     'image_thumb'             => 'products/chistorra_pavo_800/chistorra_pavo_800_thumb.png',
        //     'category_id'       => 2
        // ]);
    }
}

<?php

use App\Recipe;
use Illuminate\Database\Seeder;

class RecipeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recipe::create([
            'name'              => 'TORTA DE BERENJENA CON JAMON SERRANO',
            'full_name'         => 'Torta de Jamón Serrano Parma® con Berenjena',
            'slug'              => 'torta-de-berenjena-con-jamon-serrano',
            'difficulty'        => 'Fácil',
            'time'              => '20 Minutos',
            'introduction'      => 'Cocina fácil con Parma®. Reinventa la torta de jamón con esta receta con berenjena.',
            'ingredients'       => '
                <ul>
                    <li>2 pzas. de pan rústico de ajo </li>
                    <li>2 pzas.de  berenjena </li>
                    <li>400 g de serrano Parma® </li>
                    <li>280 g de queso manchego </li>
                    <li>50 g de hierbas de Provenza </li>
                    <li>C/s mostaza Dijon </li>
                    <li>C/s mayonesa </li>
                    <li>100 g de aceituna negra sin hueso </li>
                    <li>200 g de jitomate deshidratado </li>
                    <li>100 g de espinaca baby </li>
                    <li>C/s aceite de oliva</li>
                    <li>C/s sal gruesa </li>
                    <li>C/s sal fina</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Pelar las berenjenas y rebanar en rodajas de 1 cm de grosor. </li>
                    <li>Extender sobre un refractario poniendo una cama ligera de sal gruesa y otro poco encima de las rodajas con la finalidad de desflemarlas. </li>
                    <li>Reservar a temperatura ambiente por 30 minutos. </li>
                    <li>Picar la aceituna negra. Mezclar con la mayonesa y agregar hierbas de Provenza. </li>
                    <li>Cortar el queso en rebanadas delgadas. </li>
                    <li>Cortar el pan en 2 partes y abrir por mitad. </li>
                    <li>Una vez pasados los 30 minutos de la berenjena, escurrir el agua que soltó y enjuagar bien con agua. </li>
                    <li>En un sartén caliente con aceite de oliva freír las berenjenas a fuego medio hasta que se cuezan, esto es aproximadamente 3-4 minutos por lado. Agregar los jitomates deshidratados para dar mejor textura. </li>
                    <li>En un bowl mezclar la espinaca baby troceada con un poco de aceite de oliva y sal fina. </li>
                    <li>Untar la mezcla de mayonesa en un lado del pan y del otro, la mostaza. Colocar la berenjena como base, seguido del jitomate deshidratado, el queso, el jamón serrano Parma® y las espinacas.</li>
                    <li>Tapar la baguette y calentar unos momentos en un sartén caliente.</li>
                </ol>
            ',
            'tip'               => 'Tip Gourmand: usa queso manchego semi curado.',
            'image_thumb'       => 'recipes/torta-berenjena-jamon-serrano_thumb.png',
            'image_main'        => 'recipes/torta-berenjena-jamon-serrano_main.png',
            'video_link'        => '23pNEpkvkx0',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/torta-de-jamon/_/cat3780086?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_torta_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/torta-berenjena-jamon-serrano.pdf'
        ]);

        Recipe::create([
            'name'              => 'FUSILLI CON PEPPERONI Y CHERRY',
            'full_name'         => 'Fusilli con Pepperoni Parma® y Jitomate Cherry',
            'slug'              => 'fusilli-con-pepperoni-y-cherry',
            'difficulty'        => 'Fácil',
            'time'              => '30 Minutos',
            'introduction'      => 'Dale un toque fresco y saludable a tu comida con esta receta de pasta fusilli, pepperoni y jitomate cherry. ¿Listo para probarla?',
            'ingredients'       => '
                <ul>
                    <li>600 g de pasta fusilli </li>
                    <li>100 g de Pepperoni Parma®</li>
                    <li>60 g de aceitunas negras en rodajas</li>
                    <li>500 g de jitomate cherry </li>
                    <li>200 g de brócoli </li>
                    <li>1 cdta. de ajo picado </li>
                    <li>50 g de albahaca fresca </li>
                    <li>1 cda. de hierbas finas </li>
                    <li>C/s Queso Parmesano Parma® </li>
                    <li>C/s pimienta negra </li>
                    <li>C/s sal </li>
                    <li>C/s aceite de oliva</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cocer la pasta en agua con sal. </li>
                    <li>Cortar los jitomates y el Pepperoni Parma® por la mitad. </li>
                    <li>Cortar el brócoli en trozos pequeños. </li>
                    <li>En una cacerola a fuego medio poner el aceite, los jitomates, el ajo y el brócoli a cocer. Agregar un poco de sal y pimienta. Mover constantemente. </li>
                    <li>Cuando esté cocido el jitomate agregar la pasta, las aceitunas y el Pepperoni Parma®, dejar a fuego medio por 2 minutos más. </li>
                    <li>Servir con hojas de albahaca y espolvorear con las hierbas finas, el Queso Parmesano Parma® y un poco de pimienta negra.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/fusilli-con-pepperoni-y-cherry_thumb.png',
            'image_main'        => 'recipes/fusilli-con-pepperoni-y-cherry_main.png',
            'video_link'        => 'se3Kfc2BejU',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/fusilli/_/cat3780088?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_fusilli_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/fusilli-con-pepperoni-y-cherry.pdf'
        ]);

        Recipe::create([
            'name'              => 'BRUSHETTA DE QUESO CAMEMBERT Y SERRANO',
            'full_name'         => 'Bruschettas de Jamón Serrano Parma®',
            'slug'              => 'brushetta-de-queso-camembert-y-serrano',
            'difficulty'        => 'Fácil',
            'time'              => '12 Minutos',
            'introduction'      => 'Aprende a hacer bruschettas de jamón serrano con esta receta fácil.',
            'ingredients'       => '
                <ul>
                    <li>1 pza. de pan artesanal de costra dura </li>
                    <li>1 pza. de queso camembert </li>
                    <li>100 g de amón Serrano Parma® </li>
                    <li>1 pza. de durazno </li>
                    <li>6 hojas de hierbabuena </li>
                    <li>C/s mantequilla</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cortar el durazno y el queso en rodajas.</li>
                    <li>Cortar el pan en rebanadas medianas. </li>
                    <li>En una plancha caliente, poner mantequilla y marcar la nectarina. </li>
                    <li>Tostar el pan ligeramente y untar en la tapa un poco de mantequilla. </li>
                    <li>Sobre el pan colocar el queso, el Jamón Serrano Parma® y la nectarina.</li>
                    <li>Decorar con la hierbabuena.</li>
                </ol>
            ',
            'tip'               => 'Tip Gourmand: finalizar con una salsa de vinagre balsámico o de vino tinto.',
            'image_thumb'       => 'recipes/brushetta-de-queso-camembert-y-serrano_thumb.png',
            'image_main'        => 'recipes/brushetta-de-queso-camembert-y-serrano_main.png',
            'video_link'        => 'tTdlpgWLJKE',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/bruschettas/_/cat3780089?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_bruschettas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/brushetta-de-queso-camembert-y-serrano.pdf'
        ]);

        Recipe::create([
            'name'              => 'TACOS DE RIB EYE, TUETANO Y JAMON SERRANO',
            'full_name'         => 'Tacos de Rib Eye y Jamón Serrano Parma®',
            'slug'              => 'tacos-de-rib-eye-tuetano-y-jamon-serrano',
            'difficulty'        => 'Fácil',
            'time'              => '20 Minutos',
            'introduction'      => 'Nadie se resiste a unos tacos de carne asada. Descubre nuestra receta secreta aquí:',
            'ingredients'       => '
                <ul>
                    <li>1 pza. de rib eye </li>
                    <li>100 g de Jamón Serrano Parma® </li>
                    <li>6 pzas. de pimiento de piquillo </li>
                    <li>5 pzas. de tortillas de harina gourmet </li>
                    <li>100 g de queso panela </li>
                    <li>1 cda. de jengibre encurtido </li>
                    <li>4 pzas. de rabo de cebolla cambray </li>
                    <li>C/s sal </li>
                    <li>C/s pimienta negra </li>
                    <li>C/s aceite de oliva </li>
                    <li>C/s siracha</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cortar el rib eye en tiras delgadas. Freír en un sartén con aceite de oliva, añadir sal y pimienta. </li>
                    <li>Picar el pimiento en tiras, agregar el Jamón Serrano Parma® cortado en tiras, cocinar dos minutos más. </li>
                    <li>A parte, picar el rabo de cambray en rodajas, el jengibre en tiritas y el queso panela en cuadritos chicos, reservar. </li>
                    <li>Calentar las tortillas. </li>
                    <li>Rellenar las tortillas con la carne, decorar con el jengibre, el rabo de cambray y el queso panela. </li>
                    <li>Acompañar con salsa picante Siracha®</li>
                </ol>
            ',
            'tip'               => 'Tip Gourmand: puedes añadir tuétano sin hueso a tus tacos.',
            'image_thumb'       => 'recipes/tacos-de-rib-eye-tuetano-y-jamon-serrano_thumb.png',
            'image_main'        => 'recipes/tacos-de-rib-eye-tuetano-y-jamon-serrano_main.png',
            'video_link'        => 'BpBrEB6_hbM',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/tacos/_/cat3780090?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_tacos_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/tacos-de-rib-eye-tuetano-y-jamon-serrano.pdf'
        ]);

        Recipe::create([
            'name'              => 'ENSALADA DE AGUACATE CON SERRANO CRISPY',
            'full_name'         => 'Ensalada de Aguacate con Serrano Crujiente Parma®',
            'slug'              => 'ensalada-de-aguacate-con-serrano-crispy',
            'difficulty'        => 'Fácil',
            'time'              => '15 Minutos',
            'introduction'      => 'Come ligero con esta receta de ensalada nutritiva con aguacate y jamón serrano.',
            'ingredients'       => '
                <ul>
                    <li>18 pzas. de espárragos </li>
                    <li>400 g de Jamón Serrano Parma® </li>
                    <li>1 pza. de queso mozzarella </li>
                    <li>400 g de piñones blancos tostados </li>
                    <li>1 pza. de aguacate </li>
                    <li>2 pzas. de lechuga francesa </li>
                    <li>C/s sal </li>
                    <li>C/s pimienta blanca </li>
                    <li>C/s aceite de maíz </li>
                    <li>C/s aceite de oliva </li>
                    <li>1⁄2 tz. de vinagreta de perejil </li>
                    <li>C/s Queso Parmesano Parma® </li>
                    <li>C/s migas de pan fritas </li>
                    <li>1 mjo. de albahaca</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>En una cazuela poner aceite de maíz y calentar a fuego medio. Freír las migas de pan hasta que doren un poco. Colocar sobre papel absorbente y espolvorear Queso Parmesano Parma®. </li>
                    <li>Freír en el mismo aceite, las rebanadas de Jamón Serrano Parma® hasta que estén crujientes. Escurrir en papel absorbente, reservar. </li>
                    <li>En un sartén caliente con un poco aceite de oliva, cocer los espárragos cortados por mitad. Agregar una pizca de sal. Una vez cocidos, retirar del fuego y reservar.</li>
                    <li>Cortar la lechuga en trozos, poner en un bowl y agregar sal, pimienta y aceite al gusto y mezclar. Agregar los piñones ligeramente tostados, los espárragos, el queso mozzarella, la albahaca y el aguacate cortado en rodajas. </li>
                    <li>Colocar encima el jamón serrano crujiente y las migas de pan. </li>
                    <li>Acompañar con vinagreta de perejil.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/ensalada-de-aguacate-con-serrano-crispy_thumb.jpg',
            'image_main'        => 'recipes/ensalada-de-aguacate-con-serrano-crispy_main.png',
            'video_link'        => '0ER-Ql-jj9s',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/ensalada/_/cat3780091?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_ensalada_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/ensalada-de-aguacate-con-serrano-crispy.pdf'
        ]);

        Recipe::create([
            'name'              => 'HAMBURGUESA DE CHISTORRA Y SALAMI',
            'full_name'         => 'Hamburguesa de Chistorra y Salami Génova Parma®',
            'slug'              => 'hamburguesa-de-chistorra-y-salami',
            'difficulty'        => 'Fácil',
            'time'              => '20 Minutos',
            'introduction'      => 'Conviértete en un Food Provoker y prepara nuestra hamburguesa icónica con chistorra y salami. ',
            'ingredients'       => '
                <strong>Para la carne</strong>
                <ul>
                    <li>800 g de carne molida de res </li>
                    <li>1 pza. de huevo </li>
                    <li>1⁄4 pza. de cebolla blanca </li>
                    <li>1⁄2 diente de ajo </li>
                    <li>10 hojas de hierbabuena </li>
                    <li>2 cdas. de pan molido </li>
                    <li>100 g de Chistorra Parma® </li>
                </ul>
                <strong>Para la hamburguesa</strong>
                <ul>
                    <li>6 pzas. de bollos de hamburguesa </li>
                    <li>200 g de Salami Génova Parma® </li>
                    <li>6 rebanadas de queso gouda </li>
                    <li>6 cdas. de mayonesa de chipotle </li>
                    <li>1 pza. de aguacate </li>
                    <li>6 hojas de lechuga francesa </li>
                    <li>3 pzas. de pepinillos </li>
                    <li>2 pzas. de jitomate bola </li>
                    <li>1⁄2 pza. de cebolla morada </li>
                    <li>3 cdas. de mostaza </li>
                    <li>3 cdas. de cátsup</li>
                    <li>C/s sal</li>
                    <li>C/s pimienta blanca </li>
                    <li>C/s aceite de maíz</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cortar la chistorra en trozos pequeños y freír.</li>
                    <li>Licuar la cebolla con la hierbabuena, el ajo y 1 cucharada de aceite de oliva. </li>
                    <li>Mezclar la carne molida con la pasta de cebolla, la Chistorra Parma®, el pan molido y el huevo batido. Agregar sal y pimienta.</li>
                    <li>Cortar el jitomate en rodajas, la cebolla en plumas y trocear la lechuga y reservar en un plato. </li>
                    <li>Cocer la carne en un sartén con aceite de maíz, una vez cocida poner el queso encima para que se funda. </li>
                    <li>Untar los panes con mayonesa, cátsup y mostaza. </li>
                    <li>Poner la carne sobre el pan, el Salami Parma® y los vegetales.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/hamburguesa-de-chistorra-y-salami_thumb.png',
            'image_main'        => 'recipes/hamburguesa-de-chistorra-y-salami_main.png',
            'video_link'        => 'AzNmT4Z3seY',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/hamburguesa/_/cat3780092?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_hamburguesa_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/hamburguesa-de-chistorra-y-salami.pdf'
        ]);

        Recipe::create([
            'name'              => 'TARTITA HOJALDRE CON CHORIZO PAMPLONA Y MOZZARELLA',
            'full_name'         => 'Tartita de Hojaldre con Mozzarella y Chorizo de Pamplona Parma®',
            'slug'              => 'tartita-hojaldre-con-chorizo-pamplona-y-mozzarella',
            'difficulty'        => 'Normal',
            'time'              => '40 Minutos',
            'introduction'      => 'Receta fácil para cocinar tartitas de quezo mozzarella y chorizo español.',
            'ingredients'       => '
                <ul>
                    <li>480 g de chorizo de pamplona Parma®</li>
                    <li>6 pzas. de bases cuadradas de masa hojaldre </li>
                    <li>400 g de salsa de tomate </li>
                    <li>500 g de queso mozzarella rallado </li>
                    <li>200 g de jitomate cherry </li>
                    <li>1 pza. de aceitunas verdes en rodajas </li>
                    <li>100 g de tocino ahumado </li>
                    <li>1 pza. de pimiento verde </li>
                    <li>C/s albahaca </li>
                    <li>C/s pimienta negra </li>
                    <li>C/s aceite de maíz </li>
                    <li>1 pza. de huevo</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Picar el tocino en pequeñas rebanadas. Cocer el tocino en un sartén caliente hasta dorar. Reservar. </li>
                    <li>Cortar el pimiento en juliana. </li>
                    <li>Estirar un poco la masa.</li>
                    <li>Poner la salsa de tomate encima de cada cuadrito de masa. </li>
                    <li>Espolvorear el queso mozzarella rallado.</li>
                    <li>Colocar las rebanadas de chorizo de pamplona Parma®, el jitomate cherry cortado en mitades, las aceitunas, el tocino y el pimiento.</li>
                    <li>Batir el huevo con un chorrito de agua. Barnizar con la mezcla las orillas de la masa.</li>
                    <li>Colocar en una charola previamente engrasada y enharinada.</li>
                    <li>Hornear durante 15 a minutos a 180 °C.</li>
                    <li>Decorar con hojas enteras de albahaca.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/tartita-hojaldre-con-chorizo-pamplona-y-mozzarella_thumb.jpg',
            'image_main'        => 'recipes/tartita-hojaldre-con-chorizo-pamplona-y-mozzarella_main.png',
            'video_link'        => '-rr1IdSEcD8',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/tartita/_/cat3780093?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_tartita_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/tartita-hojaldre-con-chorizo-pamplona-y-mozzarella.pdf'
        ]);

        Recipe::create([
            'name'              => 'SINCRONIZADA CON JAMON SERRANO',
            'full_name'         => 'Sincronizadas con Jamón Serrano Parma®',
            'slug'              => 'sincronizada-con-jamon-serrano',
            'difficulty'        => 'Fácil',
            'time'              => '15 Minutos',
            'ingredients'       => '
                <ul>
                    <li>2 pzas. de tortilla de harina </li>
                    <li>200 g de Jamón Serrano Parma® </li>
                    <li>400 g de queso gouda rallado </li>
                    <li>400 g de queso manchego rallado </li>
                    <li>200 g de pollo deshebrado </li>
                    <li>2 pzas. de jitomate saladet rebanado </li>
                    <li>1⁄2 pza. de cebolla blanca </li>
                    <li>C/s salsa de chipotle </li>
                    <li>C/s aguacate</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cortar el jitomate en rodajas delgadas. </li>
                    <li>Picar la cebolla en juliana. </li>
                    <li>Mezclar los quesos. </li>
                    <li>Untar el aguacate en ambos lados de la sincronizada. </li>
                    <li>Hacer las sincronizadas con 2 rebanadas de Jamón Serrano Parma®, el pollo, los quesos, las rebanadas de jitomate y la cebolla. </li>
                    <li>Hornear por 5 minutos a 180 °C.</li>
                    <li>Servir acompañando de la salsa de chipotle.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/sincronizada-con-jamon-serrano_thumb.jpg',
            'image_main'        => 'recipes/sincronizada-con-jamon-serrano_main.png',
            'video_link'        => 'eXIiaNlT7Ko',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/sincronizadas/_/cat3780094?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_sincronizadas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/sincronizada-con-jamon-serrano.pdf'
        ]);

        Recipe::create([
            'name'              => 'CHILAQUILES CON MORITA Y CHORIZO SARTA',
            'full_name'         => 'Chilaquiles Morita con Chorizo Sarta Parma® y Queso de Cabra',
            'slug'              => 'chilaquiles-con-morita-y-chorizo-sarta',
            'difficulty'        => 'Normal',
            'time'              => '30 Minutos',
            'introduction'      => 'Prepara estos deliciosos chilaquiles con chile morita, chorizo y queso de cabra en minutos.',
            'ingredients'       => '
                <strong>Para la salsa</strong>
                <ul>
                    <li>4 pzas. de chile morita </li>
                    <li>500 g de tomate verde </li>
                    <li>1⁄2 pza. de cebolla blanca </li>
                    <li>30 g de cilantro diente ajo</li>
                    <li>C/s aceite de maíz </li>
                    <li>100 ml de caldo de pollo </li>
                    <li>C/s sal </li>
                </ul>
                <strong>Para los chilaquiles</strong>
                <ul>
                    <li>600 g de Chorizo Sarta Parma® </li>
                    <li>1⁄2 kg de totopos horneados </li>
                    <li>1⁄4 pza. de cebolla en cubos pequeños </li>
                    <li>250 ml de crema </li>
                    <li>200 g de queso de cabra </li>
                    <li>C/s cilantro para decorar</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>En una cazuela poner aceite, dorar ligeramente el ajo y la cebolla. </li>
                    <li>Agregar los tomates partidos en 4 y el chile morita. Dejar cocer 5 minutos. </li>
                    <li>Agregar el caldo y dejar hervir. </li>
                    <li>Con mucha precaución, licuar y regresar a la cazuela a fuego bajo. </li>
                    <li>Cortar el chorizo PARMA® en pequeños trozos y freír en un sartén. </li>
                    <li>Agregar los totopos a la cazuela con la salsa y mezclar. </li>
                    <li>Servir y agregar la crema, la chistorra y el queso desmoronado. </li>
                    <li>Decorar con cebolla y ramitas de cilantro.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/chilaquiles-con-morita-y-chorizo-sarta_thumb.jpg',
            'image_main'        => 'recipes/chilaquiles-con-morita-y-chorizo-sarta_main.png',
            'video_link'        => 'BSPpw9zre-4',
            'ingredients_link'  => 'https://parma.mx/productos/chistorras-y-chorizos/chorizo-sarta-parma-250g',
            'recipe_pdf'        => 'recipes/chilaquiles-con-morita-y-chorizo-sarta.pdf'
        ]);

        Recipe::create([
            'name'              => 'BROCHETAS DE SALCHICHA Y PARMESANO',
            'full_name'         => 'Brochetas Asadas de Salchicha Parma® y Piña',
            'slug'              => 'brochetas-de-salchicha-y-parmesano',
            'difficulty'        => 'Fácil',
            'time'              => '20 Minutos',
            'introduction'      => 'Cocina con Parma® unas ricas salchichas sin gluten. Atrévete a entrar a la cocina y descubre esta receta fácil de preparar.',
            'ingredients'       => '
                <ul>
                    <li>4 pzas. de Salchicha Parma®</li>
                    <li>3 rebs. de piña miel </li>
                    <li>50 g de jitomate deshidratado</li>
                    <li>C/s mostaza con miel </li>
                    <li>C/s Queso Parmesano Parma® </li>
                    <li>C/s palitos de brocheta chicos </li>
                    <li>C/s aceite</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Cortar la piña en cubos.</li>
                    <li>Cortar la salchicha en 5 partes. </li>
                    <li>Asar la salchicha y la piña en una parrilla con grill con un poco de aceite. </li>
                    <li>Crear las brochetas empezando por la piña, luego la salchicha y el jitomate. </li>
                    <li>Espolvorear Queso Parmesano Parma® encima. </li>
                    <li>Acompañar con la mostaza dulce.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/brochetas-de-salchicha-y-parmesano_thumb.jpg',
            'image_main'        => 'recipes/brochetas-de-salchicha-y-parmesano_main.png',
            'video_link'        => 'AVwDWOuxPiU',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/brochetas/_/cat3780095?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_brochetas_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/brochetas-de-salchicha-y-parmesano.pdf'
        ]);

        Recipe::create([
            'name'              => 'HUEVOS ROTOS CON SETAS Y CHISTORRA',
            'full_name'         => 'Huevos Rotos con Chistorra de Parma® y Setas',
            'slug'              => 'huevos-rotos-con-setas-y-chistorra',
            'difficulty'        => 'Normal',
            'time'              => '30 Minutos',
            'introduction'      => 'Desafía la cocina con esta receta con huevos rotos, chistorra y setas; y disfruta de un desayuno saludable con todo el sabor de Parma®.',
            'ingredients'       => '
                <ul>
                    <li>6 pzas. de huevo </li>
                    <li>2 pzas. de papa blanca </li>
                    <li>1 mjo. de cebollín </li>
                    <li>400 g de Chistorra Parma® </li>
                    <li>200 g de setas pequeñas </li>
                    <li>1 pza. de pimiento verde </li>
                    <li>C/s Queso Parmesano Parma®</li>
                    <li>C/s aceite de oliva</li>
                    <li>C/s sal fina </li>
                    <li>C/s pimienta blanca</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Pelar las papas y cortar en cubos de aproximadamente 1 cm. Cocer 5 minutos en agua con sal desde frío, reservar. </li>
                    <li>Cortar la Chistorra Parma® en rodajas de 1 cm. </li>
                    <li>Cortar el pimiento en cuadritos. </li>
                    <li>Cortar las setas en tiras medianas. </li>
                    <li>En un sartén caliente verter un poco de aceite de oliva y comenzar friendo los pimientos por un minuto, agregar la Chistorra Parma® y dorar un poco. Agregar las setas y freir por dos minutos más.</li>
                    <li>Agregar la papa, condimentar con sal y pimienta al gusto. Dejar cocer todo por dos minutos y retirar del fuego. </li>
                    <li>En otro sartén de teflón caliente, poner aceite de oliva y fría los huevos dejando la yema cruda. </li>
                    <li>En un plato semi-hondo poner la mezcla de chistorra y setas, colocar encima los huevos fritos.</li>
                    <li>Finalizar con cebollín picado finamente y el Queso Parmesano Parma®.</li>
                </ol>
            ',
            'tip'               => 'Tip Gourmand: añadir un toque de aceite de trufa blanca para finalizar.',
            'image_thumb'       => 'recipes/huevos-rotos-con-setas-y-chistorra_thumb.jpg',
            'image_main'        => 'recipes/huevos-rotos-con-setas-y-chistorra_main.png',
            'video_link'        => 'XSBZzlr18ZI',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/huevos/_/cat3780096?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_huevos_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/huevos-rotos-con-setas-y-chistorra.pdf'
        ]);

        Recipe::create([
            'name'              => 'HOT DOG SALCHICHA CHEDDAR Y CEBOLLA CARAMELIZADA',
            'full_name'         => 'Hot Dog de Salchicha Cheddar Parma®',
            'slug'              => 'hot-dog-salchicha-cheddar-y-cebolla-caramelizada',
            'difficulty'        => 'Fácil',
            'time'              => '15 Minutos',
            'introduction'      => 'Si quieres preparar un snack fácil y rápido, nuestra receta de hot dog de salchicha con queso cheddar te encantará.',
            'ingredients'       => '
                <ul>
                    <li>4 pzas. de pan para hot dog </li>
                    <li>4 pzas. de Salchicha con Cheddar Parma® </li>
                    <li>2 cdas. de mantequilla </li>
                    <li>100 g de tocino </li>
                    <li>4 cdas. de azúcar </li>
                    <li>C/s jitomate deshidratado </li>
                    <li>1 pza. de cebolla blanca </li>
                    <li>1 tz. de agua</li>
                    <li>1 frasco de col agria </li>
                    <li>2 cdas. de jalapeños </li>
                    <li>2 cdas. de mostaza de grano</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Picar la cebolla en plumas. </li>
                    <li>Enrollar la salchicha con el tocino y cruzar con un palillo para que no se abra, freír sin grasa en un sartén caliente hasta que dore y retírelos. </li>
                    <li>Aprovechar la grasa que quede en el sartén y añadir la mantequilla y la cebolla cortada en plumas.</li>
                    <li>Freír por 4 minutos, después agregar el azúcar y el agua, hervir hasta que se seque la mezcla (se hará una cebolla marrón caramelizada). Calentar la salchicha y el pan. </li>
                    <li>Embarrar en el pan la mostaza, colocar la salchicha y encima la col, la cebolla, los jalapeños y el jitomate.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/hot-dog-salchicha-cheddar-y-cebolla-caramelizada_thumb.jpg',
            'image_main'        => 'recipes/hot-dog-salchicha-cheddar-y-cebolla-caramelizada_main.png',
            'video_link'        => 'xr40K4oyfts',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/hot-dog/_/cat3780097?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_hot-dog_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/hot-dog-salchicha-cheddar-y-cebolla-caramelizada.pdf'
        ]);

        Recipe::create([
            'name'              => 'WRAP DE PECHUGA DE PAVO',
            'full_name'         => 'Wrap de Pechuga de Pavo Parma® con Queso Monterrey Jack',
            'slug'              => 'wrap-de-pechuga-de-pavo',
            'difficulty'        => 'Fácil',
            'time'              => '30 Minutos',
            'introduction'      => 'Dile adiós a tu ensalada con un wrap de Pechuga de Pavo Parma® con queso Monterrey Jack. Entra para descubrir esta receta saludable.',
            'ingredients'       => '
                <ul>
                    <li>700 g de Pechuga de Pavo Parma®</li>
                    <li>300 g de espinaca </li>
                    <li>1 pza. de pepino rallado </li>
                    <li>500 g de zanahoria rallada </li>
                    <li>200 g de queso Monterey Jack en rebanadas </li>
                    <li>1 pza. de lechuga italiana </li>
                    <li>100 g de tocino </li>
                    <li>1 pza. de nuez de la india troceada </li>
                    <li>C/s hierbabuena </li>
                    <li>C/s pimienta negra </li>
                    <li>C/s vinagreta de soya y naranja </li>
                    <li>6 pzas. de tortillas de harina para burritos</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Dora el tocino con un poco de aceite de oliva y córtalo en trozos pequeños. </li>
                    <li>Corta la lechuga, la espinaca y la hierbabuena; mezcla en un bowl con la zanahoria, el pepino, la nuez de la india, el tocino y aliña con la vinagreta. </li>
                    <li>En un sartén de teflón, calienta la Pechuga de Pavo Parma y funde encima el queso Monterey Jack.</li>
                    <li>Calienta las tortillas de harina. Encima coloca la Pechuga de Pavo Parma, el queso y el mix de lechugas. Repite el proceso para que tenga al menos dos capas de ingredientes. </li>
                    <li>Una vez envuelto, calienta en el horno durante 5 minutos y córtalo por mitad. </li>
                </ol>
                <strong>Para la carne</strong>
                <ul>
                    <li>En un frasco con 50 ml de soya, 3 cucharadas de jugo de naranja, y 10 ml de aceite de oliva virgen extra. Agítalo antes de usar.</li>
                </ul>
            ',
            'image_thumb'       => 'recipes/wrap-de-pechuga-de-pavo_thumb.jpg',
            'image_main'        => 'recipes/wrap-de-pechuga-de-pavo_main.png',
            'video_link'        => '_g3LR787yxM',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/wrap/_/cat3780098?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_wrap_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/wrap-de-pechuga-de-pavo.pdf'
        ]);

        Recipe::create([
            'name'              => 'EMPANADA DE CHORIZO SARTA CON ELOTE Y QUESO',
            'full_name'         => 'Empanada de Chorizo Parma®',
            'slug'              => 'empanada-de-chorizo-sarta-con-elote-y-queso',
            'difficulty'        => 'Fácil',
            'time'              => '35 Minutos',
            'introduction'      => 'Aprende cómo hacer empanadas con esta receta de cocina y saca el Food Provoker que llevas dentro.',
            'ingredients'       => '
                <ul>
                    <li>170 g de Chorizo Parma® </li>
                    <li>200 g de elote desgranado </li>
                    <li>300 g de espinaca </li>
                    <li>1 pza. de pimiento rojo </li>
                    <li>1 pza. de cebolla </li>
                    <li>1 diente de ajo</li>
                    <li>250 g de queso manchego rallado </li>
                    <li>1 cucharada de pimentón dulce </li>
                    <li>6 pzas. de bases redondas para empanada </li>
                    <li>200 g de salsa de tomate</li>
                    <li>C/s sal </li>
                    <li>C/s pimienta </li>
                    <li>2 pzas. de huevo </li>
                    <li>C/s aceite para freír</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Corta el diente de ajo finamente, la cebolla en pluma, el pimiento rojo en cuadritos.</li>
                    <li>En un sartén acitrona la cebolla, el elote y el pimiento. Agrega el ajo, seguido de la espinaca troceada. Después de 4 minutos, agrega el Chorizo Sarta Parma® cortado en cuadritos. </li>
                    <li>Agrega pimienta y el puré de tomate. Cuece hasta que el elote esté tierno. Reserva en frío para poder rellenar las empanadas.</li>
                    <li>Coloca las bases redondas para empanada sobre una superficie enharinada. Asegúrate que la masa esté fría. Coloca la mezcla en el centro y termina con queso manchego rallado. </li>
                    <li>Cierra la empanada utilizando el huevo como pegamento. </li>
                    <li>En una cacerola con aceite bien caliente, fríe las empanadas hasta que queden doraditas.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/empanada-de-chorizo-sarta-con-elote-y-queso_thumb.jpg',
            'image_main'        => 'recipes/empanada-de-chorizo-sarta-con-elote-y-queso_main.png',
            'video_link'        => 'KaObwoi5MyY',
            'ingredients_link'  => 'https://parma.mx/productos/chistorras-y-chorizos/chorizo-sarta-parma-250g',
            'recipe_pdf'        => 'recipes/empanada-de-chorizo-sarta-con-elote-y-queso.pdf'
        ]);

        Recipe::create([
            'name'              => 'BAGEL PECHUGA DE PAVO',
            'full_name'         => 'Bagel de Pechuga de Pavo Parma®',
            'slug'              => 'bagel-pechuga-de-pavo',
            'difficulty'        => 'Fácil',
            'time'              => '15 Minutos',
            'introduction'      => 'Aprende cómo hacer empanadas con esta receta de cocina y saca el Food Provoker que llevas dentro.',
            'ingredients'       => '
                <strong>Para el untable de queso crema</strong>
                <ul>
                    <li>1 barra de queso crema (190 g) </li>
                    <li>40 g de arándanos deshidratados picados </li>
                    <li>20 g de pistaches picados </li>
                    <li>10 g de azúcar mascabada </li>
                    <li>C/s sal </li>
                    <li>C/s pimienta negra molida</li>
                </ul>
                <strong>Para el bagel</strong>
                <ul>
                    <li>4 piezas de bagel natural o de ajonjolí (congelado) </li>
                    <li>1 cda. de agua </li>
                </ul>
                <strong>Para el relleno</strong>
                <ul>
                    <li>2 tazas de espinacas (hojas)</li>
                    <li>1 taza de germen de alfalfa</li>
                    <li>4 piezas de higo fresco rebanado </li>
                    <li>16 rebanadas de Pechuga de Pavo Parma®</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Mezclar el queso crema con los arándanos, pistaches y azúcar mascabado, sazonar con sal y pimienta al gusto.</li>
                    <li>Cortar horizontalmente por la mitad, barnizar los bagels con agua y hornear a 160 °C por 10 minutos o hasta obtener un dorado claro, retirar del horno.</li>
                    <li>Untar los bagels con la mezcla de queso crema, agregar las hojas de espinacas, el germen de alfalfa, el higo y terminar con la pechuga de pavo (4 rebanadas por bagel). Servir.</li>
                </ol>
            ',
            'tip'               => 'Tip Gourmand: se puede sustituir el higo por manzana verde finamente rebanada.',
            'image_thumb'       => 'recipes/bagel-pechuga-de-pavo_thumb.jpg',
            'image_main'        => 'recipes/bagel-pechuga-de-pavo_main.png',
            'video_link'        => 'G5VYsrYj1EM',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/bagel/_/cat3780099?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_bagel_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/bagel-pechuga-de-pavo.pdf'
        ]);

        Recipe::create([
            'name'              => 'FUSILLI CON CHISTORRA',
            'full_name'         => 'Fusilli con Salsa de Chistorra y Tomate',
            'slug'              => 'fusilli-con-chistorra',
            'difficulty'        => 'Fácil',
            'time'              => '35 Minutos',
            'introduction'      => '',
            'ingredients'       => '
                <strong>Para la salsa de chistorra y tomate</strong>
                <ul>
                    <li>1 cda. de aceite de oliva </li>
                    <li>350 g de Chistorra Parma® en trozos de 4 cm  </li>
                    <li>8 cdas. de cebolla blanca picada </li>
                    <li>1 diente de ajo picado</li>
                    <li>1 1/2 taza de tomate machacado</li>
                    <li>1/2 taza de puré de tomate </li>
                    <li>1/2 cdta. de chile rojo seco </li>
                    <li>1 cda. de albahaca fresca picada</li>
                    <li>C/s sal</li>
                    <li>C/s pimienta negra molida </li>
                </ul>
                <strong>Para la pasta </strong>
                <ul>
                    <li>400 g de fusilli </li>
                    <li>1 l de agua </li>
                    <li>10 g de sal </li>
                    <li>4 cdas. de nuez picada. </li>
                </ul>
                <strong>Para el topping crujiente</strong>
                <ul>
                    <li>1/2 taza de panko</li>
                    <li>1 cda. de aceite </li>
                    <li>1/2 taza de Queso Parmesano Parma® molido </li>
                    <li>1 cdta. de ajo en polvo</li>
                    <li>1 cda. de perejil picado</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>En aceite de oliva freír la chistorra hasta que comience a dorar, agregar la cebolla, el ajo y acitronar, incorporar el tomate  machacado, el puré de tomate y el chile rojo, cocinar por 5 minutos a fuego lento. Agregar el albahaca y sazonar con sal y pimienta  al gusto. Retirar del fuego y reservar.</li>
                    <li>Cocinar la pasta al dente en agua hirviendo con sal, drenar. Reservar. </li>
                    <li>Freír el panko en aceite a fuego medio hasta obtener un color dorado, retirar del fuego y dejar enfriar. Mezclar el panko  con el queso parmesano, ajo en polvo y perejil. </li>
                    <li>Mezclar la pasta cocida con la salsa de chistorra y la nuez hasta incorporar, servir y terminar con el topping crujiente.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/fusilli-con-chistorra_thumb.jpg',
            'image_main'        => 'recipes/fusilli-con-chistorra_main.png',
            'video_link'        => 'u5ohel_-2SA',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/fusilli-salsa/_/cat3780100?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_fusilli-salsa_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/fusilli-con-chistorra.pdf'
        ]);

        Recipe::create([
            'name'              => 'PORTOBELLO CON LAMINAS DE PARMESANO',
            'full_name'         => 'Portobello con Láminas de Queso Parmesano Parma®',
            'slug'              => 'portobello-con-laminas-de-parmesano',
            'difficulty'        => 'Fácil',
            'time'              => '20 Minutos',
            'introduction'      => '',
            'ingredients'       => '
                <ul>
                    <li>4 piezas de hongo portobello (grande) </li>
                    <li>2 cdas. de aceite de oliva</li>
                    <li>1 cdta. de ajo en polvo </li>
                    <li>C/s sal </li>
                    <li>C/s pimienta negra molida </li>
                    <li>8 cdas. de salsa de tomate </li>
                    <li>1/2 taza de albahaca fresca (hojas) </li>
                    <li>100 g de Queso Parmesano en láminas Parma® </li>
                    <li>200 g de Jamón Serrano Parma® (picado en cuadritos)</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Retirar el tronco (pie) de los hongos y picar con un tenedor por el lado de las láminas del hongo. Rociar con aceite de oliva, sazonar con ajo en polvo, sal y pimienta al gusto.</li>
                    <li>Verter 2 cucharadas de salsa de tomate a cada hongo, hojas de albahaca, láminas de queso parmesano y terminar con el jamón serrano. </li>
                    <li>Hornear a 160 °C por 10 minutos, retirar del horno y servir.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/portobello-con-laminas-de-parmesano_thumb.jpg',
            'image_main'        => 'recipes/portobello-con-laminas-de-parmesano_main.png',
            'video_link'        => '2GAr9I5qKbg',
            'ingredients_link'  => 'https://super.walmart.com.mx/promociones-especiales/bafar/portobello/_/cat3780101?utm_source=mtz_bafar&utm_medium=mtz_parmamx&utm_campaign=mtz_bafar_mf_receta_portobello_20210723&utm_content=referral&utm_term=fijo',
            'recipe_pdf'        => 'recipes/portobello-con-laminas-de-parmesano.pdf'
        ]);

        Recipe::create([
            'name'              => 'CROQUE MONSIEUR CON YORK Y PARMESANO',
            'full_name'         => 'Croque Monsieur con York y Parmesano',
            'slug'              => 'croque-monsieur-con-york-y-parmesano',
            'difficulty'        => 'Fácil',
            'time'              => '25 Minutos',
            'introduction'      => '',
            'ingredients'       => '
                <strong>Para la salsa de hongos</strong>
                <ul>
                    <li>45 g de mantequilla</li>
                    <li>4 cdas. de cebolla blanca picada </li>
                    <li>1 taza de champiñones en cuartos </li>
                    <li>1 taza de creminis rebanados </li>
                    <li>1 taza de setas en tiras </li>
                    <li>40 g de harina </li>
                    <li>1/4 taza de vino blanco</li>
                    <li> 2 1/2 de tazas leche </li>
                    <li>Sal al gusto </li>
                    <li>Pimienta negra molida al gusto </li>
                    <li>1 cda. de hierbas finas </li>
                    <li>1/2 taza de Queso Parmesano Parma® molido</li>
                </ul>
                <strong>Para el croque monsieur </strong>
                <ul>
                    <li>8 rebanadas de pan de barra</li>
                    <li>4 cdas. de mantequilla</li>
                    <li>16 rebanadas de Jamón de Pierna York Parma®</li>
                </ul>
            ',
            'process'           => '
                <ol>
                    <li>Derretir la mantequilla, agregar la cebolla y acitronar. Agregar los champiñones, creminis, setas y freír hasta que  comiencen a dorar. Añadir el harina y cocinar por 3 minutos a fuego medio moviendo constantemente. Incorporar el vino blanco y  mezclar, añadir la leche y cocinar hasta que espese, sazonar con sal y pimienta al gusto. Agregar las hierbas finas y el queso  parmesano. Reservar. </li>
                    <li>Untar 4 rebanadas de pan con 1 cucharada de mantequilla cada una, acomodar 4 rebanadas de jamón a cada pan y tapar  con las rebanadas de pan restantes.</li>
                    <li>Cubrir cada sándwich con 3/4 de taza de salsa de hongos y gratinar al horno a 200 °C por 6 minutos o hasta que esté  dorado, retirar del horno y servir.</li>
                </ol>
            ',
            'tip'               => '',
            'image_thumb'       => 'recipes/croque-monsieur-con-york-y-parmesano_thumb.jpg',
            'image_main'        => 'recipes/croque-monsieur-con-york-y-parmesano_main.png',
            'video_link'        => 'ZZLUoHM3MjE',
            'ingredients_link'  => 'https://parma.mx/productos/jamones-y-pechugas/jamon-de-pierna-york-parma-200g',
            'recipe_pdf'        => 'recipes/croque-monsieur-con-york-y-parmesano.pdf'
        ]);
    } 
}

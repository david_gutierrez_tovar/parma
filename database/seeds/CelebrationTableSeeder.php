<?php

use App\Celebration;
use Illuminate\Database\Seeder;

class CelebrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Celebration::create([
            'name' => 'MI CUMPLEAÑOS',
            'slug' => 'cumpleanos',
            'name_close' => 'FELIZ CUMPLE',
            'invitation_web' => 'experience/celebrations/mi_cumpleanos_web.jpg',
            'invitation' => 'experience/celebrations/mi_cumpleanos.pptx',

            'text_share' => 'Amigos, descubran la sorpresa especial que la #ExperienciaFoodProvoker de #Parma tiene para que celebren cada momento especial de la vida… ¡Está increíble!',
            'meta_title' => '¡Únete a la celebración aquí!',
            'meta_description' => 'Y arma tu propia experiencia Food Provoker de Parma',
            'image_share' => 'experience/celebrations/mi_cumpleanos_share.jpg'
        ]);

        Celebration::create([
            'name' => 'MI ANIVERSARIO',
            'slug' => 'aniversario',
            'name_close' => 'FELIZ ANIVERSARIO',
            'invitation_web' => 'experience/celebrations/mi_aniversario_web.jpg',
            'invitation' => 'experience/celebrations/mi_aniversario.pptx',

            'text_share' => '¡Que increíble es festejar mi #Aniversario con la #ExperienciaFoodProvoker de #Parma! Definitivamente tienen que probarla, amigos…',
            'meta_title' => '¡Únete a la celebración aquí!',
            'meta_description' => 'Y crea tu propia experiencia Food Provoker de Parma',
            'image_share' => 'experience/celebrations/mi_aniversario_share.jpg'
        ]);

        Celebration::create([
            'name' => 'TENGO UNA CENA ESPECIAL',
            'slug' => 'cena-especial',
            'name_close' => 'DISFRUTA TU CENA',
            'invitation_web' => 'experience/celebrations/cena_especial_web.jpg',
            'invitation' => 'experience/celebrations/cena_especial.pptx',

            'text_share' => 'Estoy estrenando mi casa nueva y descubrí que #Parma tiró la casa por la ventaja con la #ExperienciaFoodProvoker. ¡Pruébenla, está de lujo!',
            'meta_title' => '¡Únete a la celebración!',
            'meta_description' => 'Y haz tu propia experiencia Food Provoker de Parma',
            'image_share' => 'experience/celebrations/cena_especial_share.jpg'
        ]);

        Celebration::create([
            'name' => 'TENGO UN NUEVO EMPLEO',
            'slug' => 'nuevo-empleo',
            'name_close' => 'FELICITACIONES POR TU NUEVO EMPLEO',
            'invitation_web' => 'experience/celebrations/nuevo_empleo_web.jpg',
            'invitation' => 'experience/celebrations/nuevo_empleo.pptx',

            'text_share' => 'La #ExperienciaFoodProvoker de #Parma convirtió mi reunión en un instante inolvidable y consintió a mis invitados como nadie… ¡Descúbranla!',
            'meta_title' => '¡Únete a la celebración!',
            'meta_description' => 'Prepara tu propia experiencia Food Provoker de Parma',
            'image_share' => 'experience/celebrations/nuevo_empleo_share.jpg'
        ]);

        Celebration::create([
            'name' => 'ESTOY ESTRENANDO CASA',
            'slug' => 'estrenando-casa',
            'name_close' => 'GOZA DE TU NUEVA CASA',
            'invitation_web' => 'experience/celebrations/estrenando_casa_web.jpg',
            'invitation' => 'experience/celebrations/estrenando_casa.pptx',

            'text_share' => 'Lo mejor de mi un nuevo trabajo es el gran sabor de boca que la #ExperienciaFoodProvoker de #Parma me dejó para festejar este logro. ¡Disfrútenlo también!',
            'meta_title' => '¡Únete a la celebración!',
            'meta_description' => 'Con tu propia experiencia Food Provoker de Parma',
            'image_share' => 'experience/celebrations/estrenando_casa_share.jpg'
        ]);
    }
}

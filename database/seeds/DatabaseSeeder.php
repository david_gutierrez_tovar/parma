<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(RetailerTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductImageTableSeeder::class);
        $this->call(RecipeTableSeeder::class);
        $this->call(AboutItemTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(ProductRetailerTableSeeder::class);
        $this->call(StateRetailerTableSeeder::class);
        $this->call(CelebrationTableSeeder::class);
        $this->call(IngredientTableSeeder::class);
        $this->call(RecipeIngredientTableSeeder::class);
        $this->call(PlaylistTableSeeder::class);
        $this->call(RecipeProductTableSeeder::class);
        $this->call(RecipeRetailerTableSeeder::class);
    }
}

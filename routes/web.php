<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Home
Route::get('/', 'HomeController@index')->name('home');

// About
Route::get('/somos', 'HomeController@about')->name('about');

// Contact
Route::get('/contacto', 'HomeController@contact')->name('contact');

Route::post('/contacto/send', 'HomeController@sendMessage')->name('contact.send');

// Recipes
Route::get('/recetas', 'RecipeController@index')->name('recipes');
Route::get('/recetas/{recipe:slug}', 'RecipeController@show')->name('recipes.show');

// Retailers
Route::get('/donde-comprar', 'RetailerController@index')->name('retailers');

// Products
Route::get('/productos', 'ProductController@index')->name('products');
Route::get('/productos/{category:slug}', 'ProductController@showCategory')->name('products.category');

Route::get('/productos/{category:slug}/{product:slug}', 'ProductController@showProduct')->name('products.show');


// Experiencia
Route::get('/experiencia', 'ExperienceController@index')->name('experience');
Route::post('/create-experience', 'ExperienceController@create')->name('experience.create');

Route::get('/experiencia/{celebration:slug}', 'ExperienceController@show')->name('experience.celebration');


// API VUE
Route::get('/get-retailers', 'RetailerController@getRetailers');
Route::get('/get-productsRetailers/{product?}', 'RetailerController@getProductRetailers');

Route::get('/get-states', 'RetailerController@getStates');
Route::get('/get-stateRetailers', 'RetailerController@getStateRetailers');

Route::get('/get-products/{category?}', 'ProductController@getProducts');


Route::get('/get-playlists', 'CelebrationController@getPlaylists');

Route::get('/get-celebrations', 'CelebrationController@getCelebrations');

Route::get('/get-recipes/{category}', 'RecipeController@getRecipes');

Route::get('/get-recipe/{category}', 'RecipeController@getRecipe');
Route::get('/get-recipe-id/{recipe}', 'RecipeController@getRecipeId');
Route::get('/get-ingredients/{recipe}', 'IngredientController@getIngredients');

Route::get('/get-product/{recipe?}', 'ProductController@getProduct');


Route::get('/get-categories', 'CategoryController@getCategories');



// Admin
Route::get('/admin', function () {
    return redirect(url('nova/login'));
});
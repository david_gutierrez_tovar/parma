@extends('layouts.app')

@section('content')

    <section class="productos">

        <ul class="productos__nav">
            <a href="#serranos">SERRANOS</a>
            <a href="#chistorras-y-chorizos">CHISTORRAS <br>Y CHORIZOS</a>
            <a href="#salamis-y-madurados">SALAMIS Y <br>MADURADOS</a>
            <a href="#parmesanos">PARMESANOS</a>
            <a href="#jamones-y-pechugas">JAMONES <br>Y PECHUGAS</a>
            <a href="#salchichas">SALCHICHAS</a>
        </ul>

        <div class="productos__item" id="serranos">
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/serranos.png') }}" alt="Parma Serranos">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    SERRANOS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
        </div>
        <div class="productos__item" id="chistorras-y-chorizos">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    CHISTORRAS 
                    <br>
                    Y CHORIZOS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/chistorras.png') }}" alt="Parma Serranos">
        </div>
        <div class="productos__item" id="salamis-y-madurados">
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/salamis.png') }}" alt="Parma Serranos">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    SALAMIS 
                    <br>
                    Y MADURADOS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
        </div>
        <div class="productos__item" id="parmesanos">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    PARMESANOS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/parmesanos.png') }}" alt="Parma Serranos">
        </div>
        <div class="productos__item" id="jamones-y-pechugas">
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/jamones.png') }}" alt="Parma Serranos">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    JAMONES Y 
                    <br>
                    PECHUGAS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
        </div>
        <div class="productos__item" id="salchichas">
            <div class="productos__item-info">
                <div class="tag">Productos</div>
                <div class="name">
                    SALCHICHAS
                </div>
                <a class="btn btn-primary" href="#">
                    VER MÁS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
            </div>
            <img class="productos__item-image img-serranos" src="{{ asset('images/productos/salchichas.png') }}" alt="Parma Serranos">
        </div>


    </section>

@endsection

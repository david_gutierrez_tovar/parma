@extends('layouts.app')

@section('content')

    <section class="contact">

        <div class="container">
            <div class="contact-content">

                <div class="contact-line"></div>

                <div class="contact__info">
                    <h1>CONTÁCTANOS</h1>
                    <a class="contact__info-email" href="mailto:info@parma.mx">
                        <span class="icon-mail"></span>
                        info@parma.mx
                    </a>
                    <strong class="mt-10">¡SÍGUENOS!</strong>
                    <div class="contact__info-rss">
                        <a href=""><span class="icon-facebook"></span></a>
                        <a href=""><span class="icon-instagram"></span></a>
                        <a href=""><span class="icon-youtube"></span></a>
                    </div>
                </div>
                <div class="contact__form">
                    
                    <div class="form">
                        <form method="POST" action="{{ route('contact.send') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="name" class="form__label">NOMBRE</label>
                                <input id="name" type="text" class="form__input" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="form__label">APELLIDOS</label>
                                <input id="last_name" type="text" class="form__input" name="last_name" value="{{ old('last_name') }}" required autocomplete="name" autofocus>
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="form__label">TELÉFONO</label>
                                <input id="telephone" type="text" class="form__input" name="telephone" value="{{ old('telephone') }}" required autocomplete="name" autofocus>
                                @error('telephone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email" class="form__label">CORREO</label>
                                <input id="email" type="text" class="form__input" name="email" value="{{ old('email') }}" required autocomplete="name" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group form-group--full">
                                <label for="message" class="form__label">MENSAJE</label>
                                <textarea class="form__input" name="message" id="message">{{ old('message') }}</textarea>
                                @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif
                            <button type="submit" class="btn btn--contact">
                                ENVIAR
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                            </button>
                        </form>
                    </div>
                    
                </div>
    
            </div>
        </div>

    </section>

@endsection

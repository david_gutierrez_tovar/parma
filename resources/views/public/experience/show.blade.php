@extends('layouts.experience')

@section('content')

    @if ($experience)
        <celebration-show :experience="{{ $experience->id }}" :celebration="{{ $celebration->id - 1 }}" username="{{ $experience->name }}" :category="{{ $experience->category->id }}"></celebration-show>
    @else
        <celebration-show :celebration="{{ $celebration->id - 1 }}"></celebration-show>
    @endif

@endsection
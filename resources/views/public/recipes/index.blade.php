@extends('layouts.app')

@section('content')

    <section class="recipes">

        <div class="recipes__banner">
            <h1>RECETAS</h1>
            <img class="img-leaf" src="{{ asset('images/recipes/recipes_leaf.png') }}" alt="Parma Productos">
        </div>

        <div class="recipes__content">
            <div class="container">
                @foreach ($recipes as $recipe)
                    <a href="{{ route('recipes.show', $recipe->slug) }}" class="recipes__content-item">
                        <div class="item-img" style="background-image: url({{ asset('storage/'.$recipe->image_thumb) }})"></div>
                        <div class="item-content">
                            <div class="item-content__category">Recetas</div>
                            <div class="item-content__name">{{ strtoupper($recipe->name) }}</div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

    </section>

@endsection



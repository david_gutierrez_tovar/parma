@extends('layouts.app')

@section('content')

    <section class="recipe">

        <div class="recipe__video">
            @if ($recipe->video_link)
                <div class="recipe__video-container">
                    <div class="recipe__video-foreground">
                        <iframe src="https://www.youtube.com/embed/{{ $recipe->video_link }}?loop=1&controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1&playlist={{ $recipe->video_link }}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            @endif
        </div>

        <div class="recipe__content">
            <div class="container">
                <div class="recipe__head">
                    <div class="recipe__head-image">
                        <img src="{{ asset('storage/'.$recipe->image_main) }}" alt="{{ $recipe->full_name }}">
                    </div>
                    <div class="recipe__head-name">
                        {{-- <div class="type">Celebración</div> --}}
                        <h1>{{ mb_strtoupper($recipe->full_name) }}</h1>
                        <div class="tags">
                            <div class="tags__item">
                                <div class="tags__item-content">
                                    <img class="icon" src="{{ asset('images/recipes/icon_utensils.png') }}">
                                    <div class="text">
                                        <small>Dificultad</small>
                                        {{ mb_strtoupper($recipe->difficulty) }}
                                    </div>
                                </div>
                            </div>
                            <div class="tags__item">
                                <div class="tags__item-content">
                                    <img class="icon" src="{{ asset('images/recipes/icon_time.png') }}">
                                    <div class="text">
                                        <small>Tiempo de preparación</small>
                                        {{ mb_strtoupper($recipe->time) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if ($recipe->introduction)
                    <div class="recipe__text">{!! $recipe->introduction !!}</div>
                @endif
                

                <div class="recipe__process">
                    <div class="recipe__process-item">
                        <h2>INGREDIENTES:</h2>
                        <div class="info">
                            {!! $recipe->ingredients !!}
                        </div>
                    </div>
                    <div class="recipe__process-item">
                        <h2>PROCEDIMIENTO:</h2>
                        <div class="info">
                            {!! $recipe->process !!}
                        </div>
                    </div>
                </div>

                @if ($recipe->tip)
                    <div class="recipe__text">
                        <i>{!! $recipe->tip !!}</i>
                    </div>
                @endif

                <div class="recipe__video-element">
                    <iframe src="https://www.youtube.com/embed/{{ $recipe->video_link }}?controls=1&showinfo=0&rel=0&playlist={{ $recipe->video_link }}" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="recipe__retailers">
                    <h2>COMPRAR INGREDIENTES</h2>
                    <div class="recipe__retailers-content">
                        @foreach ($recipe->recipeRetailers as $recipeRetailer)
                            <a href="{{ $recipeRetailer->link }}" class="" target="_blank">
                                <img src="{{ asset('storage/'.$recipeRetailer->retailer->image_black) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>

        <div class="recipe__shared">
            <div class="container">
                <h2>COMPARTE ESTA RECETA:</h2>
                <div class="recipe__shared-content">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('recipes.show', $recipe->slug) }}" target="_blank"><span class="icon-facebook"></span></a>
                    <a class="singleRecipeShareBox--twitter" href="https://twitter.com/intent/tweet?text={{ $recipe->name }}+-+{{ route('recipes.show', $recipe->slug) }}" title="Comparte esta receta en Twitter" target="_blank"><span class="icon-twitter"></span></a>
                </div>
            </div>
        </div>

    </section>

@endsection



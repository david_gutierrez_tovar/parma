@extends('layouts.app')

@section('content')

    <section class="category">

        <div class="category__banner" style="background-image: url({{ asset('storage/'.$category->image_bg) }})">
            <div class="container">
                <div class="category__banner--left">
                    <span>{{ $category->presentation }}</span>
                    <H1>{{ mb_strtoupper($category->name_secondary) }}</H1>
                    <img class="stamp" src="{{ asset('storage/'.$category->image_stamp) }}" alt="{{ $category->name }}">
                </div>
                <div class="category__banner--right">
                    <h2>{{ $category->about_title }}</h2>
                    <p>
                        {!! $category->about !!}
                    </p>
                </div>
            </div>
            @if ($category->legal)
                <div class="category__banner-legal">
                    {{ mb_strtoupper($category->legal) }}
                </div>
            @endif
            
        </div>
        <div class="category__info">

            <div class="category__products">

                <h2>PRESENTACIONES</h2>
                <products-carousel :category="{{ $category->id }}"></products-carousel>

            </div>
            <div class="category__creation">
                <div class="container">
                    <h2>CREACIÓN</h2>
                    <p>
                        {{ $category->creation }}
                    </p>
                </div>
            </div>

        </div>

        <div class="category__steps">
            <div class="category__steps-container">
                <div class="category__steps-item">
                    <div class="info">
                        <div class="info__content">
                            <div class="info__frame">
                                <strong>1. {{ $category->creation_item1_title }}</strong>
                                <p>
                                    {{ $category->creation_item1_content }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="{{ asset('storage/'.$category->creation_item1_image) }}" alt="{{ $category->name }}">
                    </div>
                </div>
                <div class="category__steps-item">
                    <div class="info">
                        <div class="info__content">
                            <div class="info__frame">
                                <strong>2. {{ $category->creation_item2_title }}</strong>
                                <p>
                                    {{ $category->creation_item2_content }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="{{ asset('storage/'.$category->creation_item2_image) }}" alt="{{ $category->name }}">
                    </div>
                </div>
                <div class="category__steps-item">
                    <div class="info">
                        <div class="info__content">
                            <div class="info__frame">
                                <strong>3. {{ $category->creation_item3_title }}</strong>
                                <p>
                                    {{ $category->creation_item3_content }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="image">
                        <img src="{{ asset('storage/'.$category->creation_item3_image) }}" alt="{{ $category->name }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="category__contact">

        </div>

    </section>

@endsection



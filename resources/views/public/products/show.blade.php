@extends('layouts.app')

@section('content')

    <section class="product">

        <div class="product__banner">
            <div class="container">
                <div class="product__banner--left">
                    <div class="product__image">
                        <img class="product__image-main" src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->name }}">
                        <img class="product__image-leaf" src="{{ asset('images/hoja_producto.png') }}" alt="Hoja">
                    </div>
                </div>
                <div class="product__banner--right">
                    <H1>{{ mb_strtoupper($product->name) }}</H1>
                    <div class="presentation">{{ $product->presentation }}</div>
                    <div class="description">
                        {{ $product->description }}
                    </div>

                    {{-- Show Product Retailers --}}
                    <product-retailers :product="{{ $product->id }}"></product-retailers>

                </div>
            </div>
        </div>

        <div class="product__gallery">
            @foreach ($product->productImages()->get() as $productImage)
                <img class="product__gallery-item" src="{{ asset('storage/'.$productImage->image) }}" alt="{{ $product->name }}">
            @endforeach
        </div>

        <div class="product__related">
            <div class="container">
                <h2>PRODUCTOS SIMILARES</h2>

                {{-- Show Products Carousel --}}
                <products-carousel :category="{{ $product->category->id }}" :except="{{ $product->id }}"></products-carousel>

                {{-- <a href="{{ route('products.show', ['category' => $product->category->slug, 'product' => $product->slug]) }}" class="product__related-content">
                    @foreach ($relatedProducts as $product)
                        <div class="product__item">
                            <img class="product__item-image" src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->name }}">
                            <div class="btn btn--secondary">
                                VER MÁS
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                            </div>
                        </div>
                    @endforeach
                </a> --}}
            </div>

        </div>

    </section>

@endsection



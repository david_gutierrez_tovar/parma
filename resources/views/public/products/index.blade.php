@extends('layouts.app')

@section('content')

    <section class="productos">

        <ul class="productos__nav">
            @foreach ($categories as $category)
                <a href="#{{ $category->slug }}">{{ mb_strtoupper($category->name) }}</a>
            @endforeach
        </ul>

        @foreach ($categories as $category)
            @if ($loop->index%2 == 0)
                <div class="productos__item" id="{{ $category->slug }}">
                    <img class="productos__item-image" src="{{ asset('storage/'.$category->image_products) }}" alt="Parma Serranos">
                    <div class="productos__item-info">
                        <div class="tag">Productos</div>
                        <div class="name">
                            {{ mb_strtoupper($category->name) }}
                        </div>
                        <a class="btn btn--primary" href="{{ route('products.category', $category->slug) }}">
                            VER MÁS
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                        </a>
                    </div>
                </div>
            @else
                <div class="productos__item" id="{{ $category->slug }}">
                    <div class="productos__item-info">
                        <div class="tag">Productos</div>
                        <div class="name">
                            {{ mb_strtoupper($category->name) }}
                        </div>
                        <a class="btn btn--primary" href="{{ route('products.category', $category->slug) }}">
                            VER MÁS
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                        </a>
                    </div>
                    <img class="productos__item-image" src="{{ asset('storage/'.$category->image_products) }}" alt="Parma Serranos">
                </div>
            @endif
        @endforeach

    </section>

@endsection

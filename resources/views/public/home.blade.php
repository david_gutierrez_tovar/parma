@extends('layouts.app')

@section('content')

    <section class="home">

        <div class="home__banner">
            <div class="container">
                <div class="home__banner-left">
                    SOMOS<br>
                    PREMIUM<br>
                    SOMOS<br>
                    PARMA
                </div>
                <img class="home__banner-products" src="{{ asset('images/home_products.png') }}" alt="Productos Parma">
                <div class="home__banner-right">
                    <p>
                        Somos la marca de Grupo Bafar especializada en Madurados y Delicatessen con más de 65 años de tradición. Nuestros productos son ideales para quienes desean descubrir nuevos sabores y texturas dentro de su cocina. Inspírate y conviértete en todo un Food Provoker con todas las posibilidades que Parma tiene para ti.
                    </p>
                </div>
            </div>
        </div>
        <div class="home__products">
            <div class="container">

                @foreach ($categories as $category)
                    <a href="{{ route('products.category', $category->slug) }}" class="home__products-item">
                        <div class="item-img" style="background-image: url({{ asset('storage/'.$category->image_home) }})"></div>
                        <div class="item-content">
                            <div class="item-content__category">Productos</div>
                            <div class="item-content__name">{{ strtoupper($category->name) }}</div>
                        </div>
                    </a>
                @endforeach
            
            </div>
        </div>
        <div class="home__recipe">
            <div class="container">
                <div class="home__recipe-text">
                    Panini de jamón de pierna selección natural 
                    <br>
                    <strong>PARMA</strong>®
                </div>
                <a class="btn btn--primary" href="{{ route('recipes') }}">
                    VER TODAS
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 545.43 188.72"><polygon points="451.07 0 430.02 20.56 489.13 79.67 0 79.67 0 109.05 489.13 109.05 430.02 168.15 451.07 188.72 545.43 94.36 451.07 0"/></svg>
                </a>
                <img class="home__recipe-product" src="{{ asset('images/home/panini_jamon.png') }}" alt="Panini Parma">
            </div>
        </div>

    </section>

    {{-- Show Retailers --}}
    <retailers-carousel></retailers-carousel>

@endsection

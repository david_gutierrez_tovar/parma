@extends('layouts.app')

@section('content')

    <section class="somos">

        <div class="somos__banner">
            <h1>¿QUIÉNES SOMOS</h1>
            <img class="img-granada" src="{{ asset('images/somos/granada.png') }}" alt="Parma Productos">
        </div>

        <div class="somos__history">

            <div class="line"></div>

            {{-- <img class="img-cuchillo" src="{{ asset('images/somos/cuchillo.png') }}" alt="Parma cuchillo"> --}}

            @foreach ($about_items as $item)
                @if ($loop->index%2 == 0)
                    <div class="somos__history-item item1">
                        <div class="date">{{ $item->age }}</div>
                        <div class="left">
                            <img class="img-{{ strtolower($item->age) }}" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->age }}">
                        </div>
                        <div class="right info">
                            <p>
                                {{ $item->description }}
                            </p>
                        </div>
                    </div>
                @else
                    <div class="somos__history-item">
                        <div class="date">{{ $item->age }}</div>
                        <div class="left info">
                            <p>
                                {{ $item->description }}
                            </p>
                        </div>
                        <div class="right">
                            <img class="img-{{ strtolower($item->age) }}" src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->age }}">
                        </div>
                    </div>
                @endif
                

                
            @endforeach

        </div>

    </section>

    {{-- Show Retailers --}}
    <retailers-carousel></retailers-carousel>

@endsection

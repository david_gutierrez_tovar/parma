<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Parma</title>
    <meta name="description" content="Parma Description">

    <!-- og tags Facebook -->
    <meta property="og:title" content="Parma">
    <meta property="og:description" content="Parma Description">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://parma.mx">
    <meta property="og:image" content="{{ asset('images/social_share.jpg') }}">
    <meta property="og:site_name" content="parma.mx">
    <meta property="fb:app_id" content="1067984557070225">

    

    <!-- Twitter Cards -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@parma">
    <meta name="twitter:title" content="Parma">
    <meta name="twitter:description" content="Parma Description">
    <meta name="twitter:creator" content="@parma">
    <meta name="twitter:image:src" content="{{ asset('images/social_share.jpg') }}">
    <meta name="twitter:domain" content="parma.mx">
    <link rel="canonical" href="https://parma.mx/">

    <meta name="author" content="Parma">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="{{ asset('css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>
</head>
<body>      
    <div id="app">
        <div id="url" style="display: none;">{{ url('/') }}</div>

        <header class="header">
            <transition name="fade">
            <nav class="container header__menu" v-bind:class="{ 'show': menuShow }">
                <div class="header__menu-close" v-on:click="menuShow = false">
                    <svg id="Capa_1" enable-background="new 0 0 413.348 413.348" viewBox="0 0 413.348 413.348" xmlns="http://www.w3.org/2000/svg"><path d="m413.348 24.354-24.354-24.354-182.32 182.32-182.32-182.32-24.354 24.354 182.32 182.32-182.32 182.32 24.354 24.354 182.32-182.32 182.32 182.32 24.354-24.354-182.32-182.32z"/></svg>
                </div>
                <ul>
                    <li><a href="{{ route('about') }}">SOMOS</a></li>
                    <li><a href="{{ route('products') }}">PRODUCTOS</a></li>
                    <li><a href="{{ route('experience') }}">EXPERIENCIA</a></li>
                    
                </ul>
                <a class="header__menu-logo" href="{{ url('/') }}"><img src="{{ asset('images/logo_parma.png') }}" alt="Logo Parma"></a>
                <ul>
                    <li><a href="{{ route('recipes') }}">RECETAS</a></li>
                    <li><a href="{{ route('retailers') }}">DÓNDE COMPRAR</a></li>
                    <li><a href="{{ route('contact') }}">CONTACTO</a></li>
                </ul>
            </nav>
            </transition>
            <div class="header__mobile">
                <a class="header__mobile-logo" href="{{ url('/') }}"><img src="{{ asset('images/logo_parma.png') }}" alt="Logo Parma"></a>
                <div class="header__mobile-btn" v-on:click="menuShow = true">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g><g><path d="M501.333,96H10.667C4.779,96,0,100.779,0,106.667s4.779,10.667,10.667,10.667h490.667c5.888,0,10.667-4.779,10.667-10.667S507.221,96,501.333,96z"/></g></g>
                        <g><g><path d="M501.333,245.333H10.667C4.779,245.333,0,250.112,0,256s4.779,10.667,10.667,10.667h490.667c5.888,0,10.667-4.779,10.667-10.667S507.221,245.333,501.333,245.333z"/></g></g>
                        <g><g><path d="M501.333,394.667H10.667C4.779,394.667,0,399.445,0,405.333C0,411.221,4.779,416,10.667,416h490.667c5.888,0,10.667-4.779,10.667-10.667C512,399.445,507.221,394.667,501.333,394.667z"/></g></g>
                    </svg>
                </div>
            </div>
        </header>
        
        <main class="main">
            @yield('content')
        </main>

        <footer class="footer">
            <div class="footer__content">
                <div class="container">
                    <div class="footer__content-nav">
                        <div class="rss">
                            <a href="https://www.facebook.com/PARMAMexico/" target="_blank"><span class="icon-facebook"></span></a>
                            <a href="https://www.instagram.com/parmamexico/" target="_blank"><span class="icon-instagram"></span></a>
                            <a href="https://www.youtube.com/channel/UC5eZukx47tz-1QJg_HrZ_KQ" target="_blank"><span class="icon-youtube"></span></a>
                        </div>
                        <a href="{{ url('/') }}"><img src="{{ asset('images/logo_parma_footer.png') }}" alt="Logo Parma"></a>
                        <nav>
                            <a href="{{ route('about') }}">Somos</a>
                            <a href="{{ route('products') }}">Productos</a>
                            <a href="{{ route('recipes') }}">Recetas</a>
                            <a href="{{ route('retailers') }}">Dónde comprar</a>
                            <a href="{{ route('contact') }}">Contacto</a>
                        </nav>
                    </div>
                    <div class="footer__content-legals">
                        <div class="tyc">
                            <a href=""><small>Aviso de privacidad</small></a>
                            <a href=""><small>Términos y condiciones</small></a>
                        </div>
                        <small>
                            2020© Parma®. todos los derechos reservados.
                        </small>
                    </div>
                </div>
            </div>
        </footer>

    </div>
</body>
</html>

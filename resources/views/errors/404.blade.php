@extends('layouts.app')

@section('content')

    <section class="recipes">

        <div class="recipes__banner" style="min-height: calc(100vh - 217px); display: flex; justify-content: center; align-items: center;">
            <h1>ERROR 404</h1>
            <img class="img-leaf" src="{{ asset('images/recipes/recipes_leaf.png') }}" alt="Parma Productos">
        </div>

    </section>

@endsection



@extends('layouts.app')

@section('content')

    <section class="somos">

        <div class="somos__banner">
            <h1>¿QUIÉNES SOMOS</h1>
            <img class="img-granada" src="{{ asset('images/somos/granada.png') }}" alt="Parma Productos">
        </div>

        <div class="somos__history">

            <div class="line"></div>

            <div class="somos__history-item item1">
                <div class="date">1984</div>
                <div class="left">
                    <img class="img-cuchillo" src="{{ asset('images/somos/cuchillo.png') }}" alt="Parma cuchillo">
                    <img class="img-serrano" src="{{ asset('images/somos/serrano.png') }}" alt="Parma Serrano">
                </div>
                <div class="right info">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor, harum iure sed necessitatibus eaque doloribus voluptatibus quibusdam adipisci numquam rerum repudiandae vitae in. Reiciendis ea consequatur omnis saepe cupiditate aliquid.
                    </p>
                </div>
            </div>
            <div class="somos__history-item">
                <div class="date">1990</div>
                <div class="left info">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor, harum iure sed necessitatibus eaque doloribus voluptatibus quibusdam adipisci numquam rerum repudiandae vitae in. Reiciendis ea consequatur omnis saepe cupiditate aliquid.
                    </p>
                </div>
                <div class="right">
                    <img class="img-queso" src="{{ asset('images/somos/queso.png') }}" alt="Parma Queso">
                </div>
            </div>
            <div class="somos__history-item">
                <div class="date">2000</div>
                <div class="left">
                    <img class="img-chistorra" src="{{ asset('images/somos/chistorra.png') }}" alt="Parma Chistorra">
                </div>
                <div class="right info">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor, harum iure sed necessitatibus eaque doloribus voluptatibus quibusdam adipisci numquam rerum repudiandae vitae in. Reiciendis ea consequatur omnis saepe cupiditate aliquid.
                    </p>
                </div>
            </div>
            <div class="somos__history-item">
                <div class="date">2010</div>
                <div class="left info">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor, harum iure sed necessitatibus eaque doloribus voluptatibus quibusdam adipisci numquam rerum repudiandae vitae in. Reiciendis ea consequatur omnis saepe cupiditate aliquid.
                    </p>
                </div>
                <div class="right">
                    <img class="img-jamon" src="{{ asset('images/somos/plato_jamon.png') }}" alt="Parma Jamon">
                </div>
            </div>
            <div class="somos__history-item">
                <div class="date">HOY</div>
                <div class="left">
                    <img class="img-sandwich" src="{{ asset('images/somos/sandwich.png') }}" alt="Parma Sandwich">
                </div>
                <div class="right info">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor, harum iure sed necessitatibus eaque doloribus voluptatibus quibusdam adipisci numquam rerum repudiandae vitae in. Reiciendis ea consequatur omnis saepe cupiditate aliquid.
                    </p>
                </div>
            </div>

        </div>

    </section>

@endsection
